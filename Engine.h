#ifndef ENGINE_H
#define ENGINE_H

#include <string>
#include <map>
#include <vector>
#include <set>

#include "Ptr.h"
#include "PtrInterface.h"
#include "fwk/Exception.h"
#include "fwk/NamedInterface.h"
#include "Nominal.h"

#include "Instance.h"
#include "Activity.h"
#include "ActivityImpl.h"

using namespace ActivityImpl;

namespace Shipping {

class Segment;
class Location;
class InjectActivityReactor;

/*
 *  Entity is a base class from which Segment and Location
 *  inherit. They inherit from Entity in order to utilize
 *  Ptr<Segment> and Ptr<Location> functionality.
 */

class Entity : public Fwk::PtrInterface<Entity> {

};

class FloatValue {
public:
   double value() const { return value_; }
   bool operator==(FloatValue _opArg) const;
   bool operator!=(FloatValue _opArg) const;
   bool operator<(FloatValue _opArg) const;
   bool operator<=(FloatValue _opArg) const;
   bool operator>(FloatValue _opArg) const;
   bool operator>=(FloatValue _opArg) const;
   void valueIs(double _value) {
      value_ = _value;
   }
   FloatValue(double _value=1.0):
         value_(_value) {
   }
protected:
   double value_;
};

class IntValue {
public:
   int value() const { return value_; }
   bool operator==(FloatValue _opArg) const;
   bool operator!=(FloatValue _opArg) const;
   bool operator<(FloatValue _opArg) const;
   bool operator<=(FloatValue _opArg) const;
   bool operator>(FloatValue _opArg) const;
   bool operator>=(FloatValue _opArg) const;
   void valueIs(double _value) {
      value_ = _value;
   }
   IntValue(double _value=1.0):
         value_(_value) {
   }
protected:
   int value_;
};

class Difficulty : public Ordinal<Difficulty, double> {
public:
   Difficulty(double num=1.0) :  Ordinal<Difficulty, double>(num) {
      if((num >= 1.0) && (num <= 5.0)) { /* in range */ }
      else throw Fwk::RangeException("value=range()");
   }
};

class Miles : public Ordinal<Miles, double> {
public:
   Miles(double num=1.0) :  Ordinal<Miles, double>(num) {
      if((num >= 0.0)) { /* in range */ }
      else throw Fwk::RangeException("value=range()");
   }
};

class MilesPerHour : public Ordinal<MilesPerHour, double> {
public:
   MilesPerHour(double num=1.0) :  Ordinal<MilesPerHour, double>(num) {
      if((num >= 0.0)) { /* in range */ }
      else throw Fwk::RangeException("value=range()");
   }
};

class DollarsPerMile : public Ordinal<DollarsPerMile, double> {
public:
   DollarsPerMile(double num=1.0) :  Ordinal<DollarsPerMile, double>(num) {
      if((num >= 0.0)) { /* in range */ }
      else throw Fwk::RangeException("value=range()");
   }
};

class Packages : public Ordinal<Packages, unsigned int> {
public:
   Packages(unsigned int num=1.0) :  Ordinal<Packages, unsigned int>(num) {
      if((num >= 1)) { /* in range */ }
      else throw Fwk::RangeException("value=range()");
   }
};

class Dollars : public Ordinal<Dollars, double> {
public:
   Dollars(double num=1.0) :  Ordinal<Dollars, double>(num) {
      if((num >= 0.0)) { /* in range */ }
      else throw Fwk::RangeException("value=range()");
   }
};

class Hours : public Ordinal<Hours, double> {
public:
   Hours(double num=1.0) :  Ordinal<Hours, double>(num) {
      if((num >= 0.0)) { /* in range */ }
      else throw Fwk::RangeException("value=range()");
   }
};

class Path : public Entity {
public:
    std::vector<Ptr<Entity> > *path();
    void pathIs(std::vector<Ptr<Entity> > *path);

    /*
     * Takes in an empty path called "copy". For copy,
     * a new path vector is created and populated with
     * every Ptr<Entity> in order from this path.
     */

    void pathCopy(Path& copy);

protected:
    std::vector<Ptr<Entity> > *path_;
};

enum Mode {
  truck_ = 0,
  boat_ = 1,
  plane_ = 2,
};

static inline Mode truck() { return truck_; }
static inline Mode boat() { return boat_; }
static inline Mode plane() { return plane_; }

class Location : public Entity {
public:
    Location();
    void nameIs(const string name);
    string name();
    
    std::vector<Ptr<Segment> >* segments();
    Ptr<Segment> segment(const unsigned int segmentNumber);
    void segmentIs(Ptr<Segment> segment);
    void segmentDel(Ptr<Segment> segment);

    Hours averageLatency() { return totalLatency_.value() / numShipmentsRecieved_; }
    int numShipmentsRecieved() { return numShipmentsRecieved_; }
    Dollars totalCost() { return totalCost_; }

    void onArrival(Hours latency, Dollars cost);

protected:
    string name_;
    std::vector<Ptr<Segment> > segments_;
    
    int numShipmentsRecieved_;
    Hours totalLatency_;
    Dollars totalCost_;

};

class ShipmentActivity;

class Segment : public Entity {
public:

    Segment();
    enum ExpediteSupport {
        no_ = 0,
        yes_ = 1,
    };
    
    static inline ExpediteSupport no() { return no_; }
    static inline ExpediteSupport yes() { return yes_; }

    void nameIs(const string name);
    string name();

    void modeIs(Mode mode);
    Mode mode();
    
    void sourceIs(Ptr<Location> location);
    Ptr<Location> source();
    
    void lengthIs(Miles length);
    bool lengthInitialized();
    Miles length();
    
    void returnSegmentIs(Ptr<Segment> returnSegment);
    Ptr<Segment> returnSegment();
    
    void capacityIs(int capacity);
    int capacity();
    
    void difficultyIs(Difficulty difficulty);
    bool difficultyInitialized();
    Difficulty difficulty();
    
    void expediteSupportIs(ExpediteSupport expediteSupport);
    bool expediteSupportInitialized();
    ExpediteSupport expediteSupport();

    int numShipmentsRecieved();
    void onShipment(Ptr<ShipmentActivity>);
    
private:
    string name_;
    Miles length_;
    Mode mode_;
    Ptr<Location> source_;
    Difficulty difficulty_;
    ExpediteSupport expediteSupport_;
    Ptr<Segment> returnSegment_;
    int numShipmentsRecieved_;
    int capacity_;

    bool lengthInit_, difficultyInit_, expediteInit_;
};

class Terminal : public Location {

public:
    void modeIs(Mode mode);
    Mode mode();
    
private:
    Mode mode_;
};

class Port : public Location {
};

class Customer : public Location {
public:
    Customer();
    class Notifiee : public Fwk::BaseNotifiee<Customer> {
        public:
        typedef Fwk::Ptr<Notifiee> Ptr;

            Notifiee(Customer* cust) : Fwk::BaseNotifiee<Customer>(cust) {}

        virtual void onRateSet() {}
        virtual void onSizeSet() {}
        virtual void onDestSet() {}
    };
    
    void rateIs(double rate);
    double rate();
    void destIs(Ptr<Customer> dest);
    Ptr<Customer> dest();
    void sizeIs(int size);
    double size();
    
    void lastNotifieeIs(Ptr<Notifiee> n);
    Ptr<Notifiee> notifiee();
    
    void injectorIs(Fwk::Ptr<InjectActivityReactor> injector);
    Fwk::Ptr<InjectActivityReactor> injector();
    
private:
    double rate_;
    Ptr<Customer> dest_;
    int size_;
    Ptr<Notifiee> notifiee_;
    Ptr<InjectActivityReactor> injector_;
};

class Fleet : public Entity {
public:

    // Given a transporation mode, returns its "speed", "cost" or "capacity" (attr)
    
    Fleet();

    void speedIs(Mode mode, MilesPerHour speed);
    void costIs(Mode mode, Dollars cost);
    void capacityIs(Mode mode, Packages capacity);

    MilesPerHour speed(Mode mode);
    Dollars cost(Mode mode);
    Packages capacity(Mode mode);
    
    bool speedInitialized(Mode mode);
    bool costInitialized(Mode mode);
    bool capacityInitialized(Mode mode);

private:
    std::map<Mode, double> modeToSpeedMap_;
    std::map<Mode, double> modeToCostMap_;
    std::map<Mode, int> modeToCapacityMap_;
    
    std::map<Mode, bool> modeSpeedInitialized_;
    std::map<Mode, bool> modeCostInitialized_;
    std::map<Mode, bool> modeCapacityInitialized_;
};

/*
 * Used for connect functionality, which returns a
 * vector of PathInfo structs, each containing a Path,
 * as well as the corresponding cost, time, and whether
 * or not the given path is expedited.
 */

struct PathInfo {
   Path path;
   Dollars cost;
   Hours time;
   Segment::ExpediteSupport expedited;
};

class Pathfinder;
class Engine;
class ShipmentActivity;

class ShippingManager : public ActivityImpl::ManagerImpl {

public:
    void engineIs(Fwk::Ptr<Engine> e);
    Fwk::Ptr<Engine> engine();
    void nowIs(Time t);

protected:
    Fwk::Ptr<Engine> engine_;

private:
    void shipmentActivityArrived(Fwk::Ptr<ShipmentActivity> shipment);
};

class RealTimeManager : public Entity{

public:
    void realTimeIs(Time t);
    Time realTime();
    void shippingManagerIs(Ptr<ShippingManager> shippingManager);

private:
    Time time_;
    Ptr<ShippingManager> shippingManager_;
};

class Engine : public Fwk::NamedInterface {

public:

class StatNotifiee;
class Shipping::Pathfinder;

Engine() : Fwk::NamedInterface("Engine") {
    fleet_ = new Fleet;
    shippingManager_ = new ShippingManager;
    realTimeManager_ = new RealTimeManager;

    Ptr<Engine> thisEngine = Ptr<Engine>(this);
    shippingManager_->engineIs(thisEngine);
    realTimeManager_->shippingManagerIs(shippingManager_);
}

Ptr<Entity> instanceNew(std::string name, std::string type);
void instanceDel(std::string name);

Ptr<Location> location(std::string name);
Ptr<Segment> segment(std::string name);
Ptr<Activity> activity(std::string);
void activityIs(std::string, Ptr<Activity>);
int numShipments(std::string segment);
void numShipmentsIs(std::string segment, int num);

Dollars pathCost(Path& path);
Ptr<Pathfinder> pathfinder();
Ptr<ShippingManager> shippingManager();
Ptr<RealTimeManager> realTimeManager();

std::vector<Ptr<Location> > locations();
std::vector<Ptr<Segment> > segments();

void fleetIs(Ptr<Fleet> fleet) { fleet_ = fleet; }
Ptr<Fleet> fleet() { return fleet_; }

private:

std::map<std::string, Ptr<Location> > locationRecord_;
std::map<std::string, Ptr<Segment> > segmentRecord_;
std::map<std::string, Ptr<Activity> > activeSegments_;
std::map<std::string, int> numShipments_;

protected:
// stores a pointer to the ManagerImpl that is Engine's Notifier
Ptr<Fleet> fleet_;
Ptr<Pathfinder> pathFinder_;

Engine::StatNotifiee *statNotifiee_;

void statNotifieeIs(Engine::StatNotifiee *notifiee) {
    Engine *me = const_cast<Engine *>(this);
    me->statNotifiee_ = notifiee;
}

Ptr<ShippingManager> shippingManager_;
Ptr<RealTimeManager> realTimeManager_;

};

class Engine::StatNotifiee {
public:
    virtual void onInstanceNew(Ptr<Entity> entity, std::string name, std::string type){}
    virtual void onInstanceDel(Ptr<Entity> entity, std::string name, std::string type){}
    StatNotifiee(Ptr<Engine> engine);
};


class Statistics : public Entity {
public: 
    
    Statistics(Ptr<Engine> engine);
    // Returns the number of entities of the given type that exist in the network
    int entities(std::string type); 

    // Returns perecentage of segments that offer expedited shipping
    double percentSegmentsWithExpeditedShipping();

    void onInstanceNew(Ptr<Entity> entity, std::string name, std::string type);
    void onInstanceDel(Ptr<Entity> entity, std::string name, std::string type);

private:
    Ptr<Engine> engine_;
    std::map<std::string, int> typeCountMap_;
    std::vector<std::string> segmentList_;
};

class StatisticsReactor : public Engine::StatNotifiee {
public:
    void onInstanceNew(Ptr<Entity> entity, std::string name, std::string type) {
        stats_->onInstanceNew(entity, name, type);
    }

    void onInstanceDel(Ptr<Entity> entity, std::string name, std::string type) {
        stats_->onInstanceDel(entity, name, type);
    }

    // To be called by Instance once Statistics and Engine are created. Creates a
    // new StatisticsReactor, which listens for instanceNew and instanceDel calls
    // to Engine, and updates Statistics accordingly.

    static StatisticsReactor *StatisticsReactorIs(Ptr<Statistics> s, Ptr<Engine> e) {
        StatisticsReactor *sr = new StatisticsReactor(s,e);
        return sr;
    }

protected:
    StatisticsReactor(Ptr<Statistics> s, Ptr<Engine> e) : Engine::StatNotifiee(e) {
        stats_ = s;
    }
    Ptr<Statistics> stats_;
};

class Connectivity : public Entity {
public:

    Connectivity(Ptr<Engine> engine);
    // Returns a vector of all paths (also represented as a vector of segments) 
    // where each path satisfies the constraints passed in as parameters. 
    // Sentinels are required if the parameter is not to be constrained
    void pathsWithConstraints(
        std::vector<Path>& allPaths,
        std::string sourceName, 
        Miles distance, 
        Dollars cost, 
        Hours time, 
        Segment::ExpediteSupport expedited,
        std::set<std::string> constraints
    );

    // Returns a vector of all paths (also represented as a vector of segments) 
    // where each path satisfies the constraints passed in as parameters
    std::vector<PathInfo> *pathsBetweenLocations(
        std::string sourceName, 
        std::string destName
    );

private:

    /* pathsWithConstraints is a function that recurses through all paths that
     * satisfy the given constraints, adding paths as applicable to allPaths, the
     * list of all valid paths that satisfy the constraints. pathWithConstraintsRecursive
     * is the recursive implementation of this function.
     */

    void pathsWithConstraintsRecursive(
        std::vector<Path>& allPaths,
        Path& path,
        Ptr<Location> source,
        Ptr<Location> current,
        Miles distance,
        Dollars cost,
        Hours time,
        Segment::ExpediteSupport expedited,
        std::set<std::string>& constraints,
        Miles curDistance,
        Dollars curCost,
        Hours curTime
   );

    /*
     * The recursive implementation of pathsBetweenLocations.
     */

    void pathsBetweenLocationsRecursive(
        std::vector<Path>& allPaths,
        Path& path,
        Ptr<Location> start,
        Ptr<Location> current,
        Ptr<Location> dest
    );

    /*
     * Used in connect functionality: once a vector of Paths that connect
     * the two given nodes is found, pathInfoForPaths fills in the necessary
     * information about their cost, time, and if the path is expedited.
     *
     * If a path can be entirely expedited, two paths are added to the return
     * vector: one that is expedited and one that isn't.
     */

    std::vector<PathInfo> *pathInfoForPaths(std::vector<Path> &paths);

    Ptr<Engine> engine_;
};

class Pathfinder : public Entity {
public:
    Pathfinder(Ptr<Engine> engine);
    Path path(Ptr<Location> source, Ptr<Location> dest, bool minCost);
private:
    struct SpanningTreeLocation {
        Ptr<Location> location;
        std::vector<Ptr<Segment> > spanningTreeSegments;
    };

    Ptr<Engine> engine_;
    std::map<std::string, SpanningTreeLocation> minCostSpanningTree_;
    std::map<std::string, SpanningTreeLocation> minDistSpanningTree_;

    bool pathRecursive(
        Path &curPath, 
        Ptr<Location> dest, 
        std::set<std::string>& visited, 
        bool minCost
    );

    void forestMerge(
       std::map<std::string, std::set<std::string> >& forest,
       Ptr<Segment> nextSegment,
       bool minCost
    );

    double segmentCost(Ptr<Segment> segment, bool minCost);
    
    std::vector<Ptr<Segment> > insertionSort(
        std::vector<Ptr<Segment> >& unsorted, 
        bool minCost
    );
};

class ShipmentActivity : public ActivityImpl::ActivityImpl {

public:
    ShipmentActivity(string name, Fwk::Ptr<ManagerImpl> manager, Time start) : ActivityImpl(name, manager), startTime_(start) {};
    void numPackagesIs(int n);
    int numPackages();
    void finalDestIs(Fwk::Ptr<Location> dest);
    Fwk::Ptr<Location> finalDest();
    void curDestIs(Fwk::Ptr<Location> dest);
    Fwk::Ptr<Location> curDest();
    Fwk::Ptr<Segment> curSegment();
    Fwk::Ptr<Segment> nextSegment();
    void atNextSegment();
    void atNextLocation();
    void sourceIs(Fwk::Ptr<Customer> s);
    Fwk::Ptr<Customer> source();
    Time startTime();
    bool atDest();

    void pathIs(Path path) { 
        path_ = path;
        finalDest_ = dynamic_cast<Customer*>(path.path()->at(path.path()->size() - 1).ptr());
        
        std::vector<Fwk::Ptr<Entity> > * pathVec = path.path();
        cout << "-------------" << endl;
        for(int i = 0; i < pathVec->size(); i++) {
            Fwk::Ptr<Location> curLoc = dynamic_cast<Location*>(pathVec->at(i).ptr());
            if(curLoc) {
                cout << curLoc->name() << " - ";
            }
        }
        cout << endl;
        cout << "===============" << endl;
        
        
    }
    Path path() { return path_; }
    void pathCostIs(Dollars cost) { pathCost_ = cost; }
    Dollars pathCost() { return pathCost_; }

protected:
    int numPackages_;
    Path path_;
    Dollars pathCost_;
    Time startTime_;
    Fwk::Ptr<Location> finalDest_;
    Fwk::Ptr<Location>  curDest_;
    Fwk::Ptr<Segment> curSegment_;
    Fwk::Ptr<Customer>  source_; 
};

class ShipmentInjectActivity : public ActivityImpl::ActivityImpl {

public:
    ShipmentInjectActivity(string name, Fwk::Ptr<ManagerImpl> manager) 
        : ActivityImpl(name, manager){}
    
protected:
};

class CustomerReactor : public Customer::Notifiee {

public:
    CustomerReactor(Fwk::Ptr<ActivityImpl::ManagerImpl> manager, Customer*
			 c) 
     : Notifiee(c), manager_(manager), rateSet_(false), sizeSet_(false), destSet_(false), customer_(c){}
    void onRateSet();
    void onSizeSet();
    void onDestSet();

 protected:
    void checkAllSet();
    Fwk::Ptr<ActivityImpl::ManagerImpl> manager_;
    Fwk::Ptr<Customer> customer_;
    
    bool rateSet_;
    bool sizeSet_;
    bool destSet_;
};

class InjectActivityReactor : public Activity::Notifiee {

public:
    InjectActivityReactor(Fwk::Ptr<ActivityImpl::ManagerImpl> manager, Activity*
			 activity) 
     : Notifiee(activity), activity_(activity), manager_(manager) {}
     
    void sourceIs(Fwk::Ptr<Customer> source);
    Fwk::Ptr<Customer> source();
    void onStatus();
    void onNextTime();
    
private:
    Fwk::Ptr<Customer> source_;
    ShipmentInjectActivity::Ptr activity_;
    Fwk::Ptr<ActivityImpl::ManagerImpl> manager_;
    Path path_;
    Dollars pathCost_;
};

} /* end namespace */

#endif
