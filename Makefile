CXXFLAGS = -Wall -g

OBJECTS = Instance.o Engine.o ActivityImpl.o
LIBS = fwk/BaseCollection.o fwk/BaseNotifiee.o fwk/Exception.o

default:	test1 test2 example reactorExample sample-client verification experiment

test1:	test1.o $(OBJECTS) $(LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $^

test2:  test2.o $(OBJECTS) $(LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $^

example:	example.o $(OBJECTS) $(LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $^
	
reactorExample:	reactorExample.o $(OBJECTS) $(LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $^
	
sample-client:	sample-client.o $(OBJECTS) $(LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $^
	
verification:	verification.o $(OBJECTS) $(LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $^
	
experiment:	experiment.o $(OBJECTS) $(LIBS)
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -f test1 test1.o $(OBJECTS) $(LIBS) *~
	rm -f test2 test2.o $(OBJECTS) $(LIBS) *~
	rm -f example example.o $(OBJECTS) $(LIBS) *~
	rm -f sample-client sample-client.o $(OBJECTS) $(LIBS) *~
	rm -f verification verification.o $(OBJECTS) $(LIBS) *~
	rm -f experiment experiment.o $(OBJECTS) $(LIBS) *~

ActivityImpl.o: ActivityImpl.h Activity.h
Engine.o: Engine.cpp Engine.h
Instance.o: Instance.cpp Instance.h PtrInterface.h Ptr.h Engine.cpp Engine.h
test1.o: test1.cpp Instance.h PtrInterface.h Ptr.h
test2.o: test2.cpp Instance.h PtrInterface.h Ptr.h
sample-client.o: sample-client.cpp Instance.h PtrInterface.h Ptr.h
verification.o: verification.cpp Instance.h PtrInterface.h Ptr.h
experiment.o: experiment.cpp Instance.h PtrInterface.h Ptr.h
