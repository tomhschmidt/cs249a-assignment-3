#include <stdlib.h>
#include <cstdio>
#include <iostream>
#include <map>
#include <typeinfo>
#include <vector>
//#include "Activity.h"
#include "Instance.h"
#include "Engine.h"
#include "fwk/Exception.h"

namespace Shipping {

using namespace std;

class StatisticsRep;
class ConnectivityRep;
class FleetRep;

//
// Rep layer classes
//

string doubleToString(double value) {
    char buffer[256];
    snprintf(buffer, sizeof(buffer), "%.2f", value);
    return string(buffer);
}

string intToString(int value) {
    std::ostringstream s;
    s << value;
    return s.str();
}

class ManagerImpl : public Instance::Manager {
public:
    ManagerImpl();

    // Manager method
    Ptr<Instance> instanceNew(const string& name, const string& type);

    // Manager method
    Ptr<Instance> instance(const string& name);

    // Manager method
    void instanceDel(const string& name);
    
    Ptr<Engine> engine();

private:
    map<string,Ptr<Instance> > instance_;
    Ptr<StatisticsRep> stats_;
    Ptr<ConnectivityRep> conn_;
    Ptr<FleetRep> fleet_;
    Ptr<Engine> engine_;
};

class LocationRep : public Instance {
public:

    LocationRep(const string& name, ManagerImpl* manager) :
        Instance(name), manager_(manager)
    {
        manager_ = manager;
    }
    
    string name();

    // Instance method
    string attribute(const string& name);
    
    Ptr<Location> location();

    // Instance method
    void attributeIs(const string& name, const string& v);

protected:
    Ptr<Location> location_; 
    Ptr<ManagerImpl> manager_;
    string name_;
    
    int segmentNumber(const string& name);

};

string LocationRep::name() {
    return name_;
}

string LocationRep::attribute(const string& name) {
    int i = segmentNumber(name);
    
    if (i != 0) {
        Ptr<Segment> segment = location_->segment(i);
        if(segment) {
            return segment->name();
        } 
        else {
            cerr << "Segment " << i << " does not exist" << endl;
        }
    } 
    else {
        cerr << "Invalid segment name " << endl;
    }

    return "";
}

void LocationRep::attributeIs(const string& name, const string& v) {

}

Ptr<Location> LocationRep::location() {
    return location_;
}

static const string segmentStr = "segment";
static const int segmentStrlen = segmentStr.length();

int LocationRep::segmentNumber(const string& name) {
    if (name.substr(0, segmentStrlen) == segmentStr) {
        const char* t = name.c_str() + segmentStrlen;
        return atoi(t);
    }
    
    return 0;
}
                                                                                                  
class TerminalRep : public LocationRep {
public:

    TerminalRep(const string& name, const string& type, ManagerImpl *manager) :
        LocationRep(name, manager)
    {
        location_ = Ptr<Location>(dynamic_cast<Location*>(manager_->engine()->instanceNew(name, type).ptr()));
    }

};

class PortRep : public LocationRep {
public:

    PortRep(const string& name, ManagerImpl *manager) :
        LocationRep(name, manager)
    {
        location_ = Ptr<Location>(dynamic_cast<Location*>(manager_->engine()->instanceNew(name, "Port").ptr()));
    }

};

class CustomerRep : public LocationRep {
public:

    CustomerRep(const string& name, ManagerImpl *manager) :
        LocationRep(name, manager)
    {
        location_ = Ptr<Location>(dynamic_cast<Location*>(manager_->engine()->instanceNew(name, "Customer").ptr()));
    }
    
    // Instance method
    string attribute(const string& name);

    // Instance method
    void attributeIs(const string& name, const string& v);

};

string CustomerRep::attribute(const string& name) {
    Ptr<Customer> customer = Ptr<Customer>(dynamic_cast<Customer*>(location_.ptr()));
    if(name == "Transfer Rate") {
        return doubleToString(customer->rate());
    } else if(name == "Shipment Size") {
        return intToString(customer->size());
    } else if(name == "Destination") {
        if(customer->dest()) {
            return customer->dest()->name();
        } else {
            return "";
        }
    } else if(name == "Num Shipments") {
        return intToString(customer->numShipmentsRecieved());
    } else if(name == "Average Latency") {
        return doubleToString(customer->averageLatency().value());
    } else if(name == "Total Cost") {
        return doubleToString(customer->totalCost().value());
    } else {
        cerr << "Invalid attribute name " << endl;
        return "";
    }
}

void CustomerRep::attributeIs(const string& name, const string& v) {
    Ptr<Customer> customer = Ptr<Customer>(dynamic_cast<Customer*>(location_.ptr()));
    if(name == "Transfer Rate") {
        double value = atof(v.c_str());
        if (value > 0) {
            customer->rateIs(value);
        } else {
            cerr << "Invalid value for Transfer Rate " << v << endl;
            throw Fwk::RangeException("Invalid value for Transfer Rate ");
        }
    } else if(name == "Shipment Size") {
        int value = atoi(v.c_str());
        if (value > 0) {
            customer->sizeIs(value);
        } else {
            cerr << "Invalid value for Shipment Size" << endl;
            throw Fwk::RangeException("Invalid value for Shipment Size ");
        }
    } else if(name == "Destination") {
        Ptr<CustomerRep> destination = Ptr<CustomerRep>(dynamic_cast<CustomerRep*>(manager_->instance(v).ptr()));
        if(destination) {
           customer->destIs(Ptr<Customer>(dynamic_cast<Customer*>(destination->location().ptr())));
        } else {
            cerr << "Could not find Customer name " << v << endl;
            throw Fwk::EntityNotFoundException( "Could not find Customer name ");
        } 
    }
}

class SegmentRep : public Instance {
public:

    SegmentRep(const string& name, const string& type, ManagerImpl* manager) :
        Instance(name), manager_(manager)
    {
        name_ = name;
        manager_ = manager;
        segment_ = Ptr<Segment>(dynamic_cast<Segment*>(manager_->engine()->instanceNew(name, type).ptr()));
    }
    
    string name();

    // Instance method
    string attribute(const string& name);

    // Instance method
    void attributeIs(const string& name, const string& v);
    
    Ptr<Segment> segment();

private:
    Ptr<ManagerImpl> manager_;
    string name_;
    
    Ptr<Segment> segment_;

};
      
string SegmentRep::name() {
    return name_;
}

string SegmentRep::attribute(const string& name) {
    if(name == "source") {
        if(segment_->source()) {
            return segment_->source()->name();
        } else {
            return "";
        }
    } else if(name == "length") {
        if(segment_->lengthInitialized()) {
            Miles length = segment_->length();
            return doubleToString(length.value());
        } else{
            return "";
        }
    } else if(name == "return segment") {
        if(segment_->returnSegment()) {
            return segment_->returnSegment()->name();
        } else {
            return "";
        }
    } else if(name == "difficulty") {
        if(segment_->difficultyInitialized()) {
            Difficulty difficulty = segment_->difficulty();
            return doubleToString(difficulty.value());
        } else {
            return "";
        }
    } else if(name == "expedite support") {
        if(segment_->expediteSupportInitialized()) {
            Segment::ExpediteSupport expedite = segment_->expediteSupport();
            if(expedite == Segment::yes()) {
                return "yes";
            } else {
                return "no";
            }
        } else {
            return "";
        }
    } else if(name == "capacity") {
        return intToString(segment_->capacity());
    } else if(name == "Shipments Received") {
        return intToString(segment_->numShipmentsRecieved());
    }
    else {
        cerr << "Invalid attribute" << endl;
        return "";
    }
}

void SegmentRep::attributeIs(const string& name, const string& v) {
    if(name == "source") {
        if(segment_->source()) {
            segment_->source()->segmentDel(segment_);
            segment_->sourceIs(NULL);
        }
        
        Ptr<LocationRep> source = Ptr<LocationRep>(dynamic_cast<LocationRep*>(manager_->instance(v).ptr()));
        if(source) {
            Ptr<Terminal> terminal = Ptr<Terminal>(dynamic_cast<Terminal*>(source->location().ptr()));
            if(terminal){
                if(terminal->mode() == segment_->mode()) {
                    segment_->sourceIs(source->location());
                    source->location()->segmentIs(segment_);
                } else {
                    cerr << "Segment source mode does not match segment mode " << endl;
                    throw Fwk::RangeException("Segment source mode does not match segment mode ");
                }
            } else {
                segment_->sourceIs(source->location());
                source->location()->segmentIs(segment_);
            }
        } else {
            cerr << "Location name " << v << " does not exist " << endl;
            throw Fwk::EntityNotFoundException("Segment source mode does not match segment mode ");
        } 
    } else if(name == "length") {
        double value = atof(v.c_str());
        if(value > 0) {
            Miles length = Miles(value);
            segment_->lengthIs(length);
        } else {
            cerr << "Invalid value of " << value << " for attribute " << name << endl;
            throw Fwk::RangeException("Invalid value for attribute length ");
        }
       
    } else if(name == "return segment") {
        // Find segment and set
        Ptr<SegmentRep> returnSegment = Ptr<SegmentRep>(dynamic_cast<SegmentRep*>(manager_->instance(v).ptr()));
        if(returnSegment) {
            if(returnSegment->segment()->mode() == segment_->mode()) {
                segment_->returnSegmentIs(returnSegment->segment());
                returnSegment->segment()->returnSegmentIs(segment_);
            }
            else {
                cerr << "Return segment has different mode of transporation " << endl;
                throw Fwk::RangeException("Return segment has different mode of transporation ");
            }
        } else {
            cerr << "Could not find Segment " << v << endl;
            throw Fwk::RangeException("Could not find Segment");
        }
    } else if(name == "difficulty") {
        double value = atof(v.c_str());
        if(value >= 1.0 && value <= 5.0) {
            Difficulty difficulty = Difficulty(atof(v.c_str()));
            segment_->difficultyIs(difficulty);
        } else {
            cerr << "Invalid value of " << value << " for attribute " << name << endl;
            throw Fwk::RangeException("Invalid value for Difficulty");
        }
    } else if(name == "expedite support") {
        if(v == "no") {
            segment_->expediteSupportIs(Segment::no());
        } else if(v == "yes") {
            segment_->expediteSupportIs(Segment::yes());
        } else {
            cerr << "Invalid value " << v << " for ExpediteSupport" << endl;
            throw Fwk::RangeException("Invalid value for ExpediteSupport");
        }
    }  else if(name == "capacity") {
        int value = atoi(v.c_str());
        if(value > 0) {
            segment_->capacityIs(value);
        } else {
            cerr << "Invalid value " << v << " for Capacity" << endl;
            throw Fwk::RangeException("Invalid value for Capacity");
        }
    } else {
        cerr << "Invalid attribute" << endl;
    }
}

Ptr<Segment> SegmentRep::segment() {
    return segment_;
}

class StatisticsRep : public Instance {
public:

    StatisticsRep(const string& name, ManagerImpl* manager) :
        Instance(name), manager_(manager)
    {
        manager_ = manager;
        stats_ = new Statistics(manager_->engine());
        StatisticsReactor::StatisticsReactorIs(stats_, manager_->engine());
    }
    
    string name();

    // Instance method
    string attribute(const string& name);

    // Instance method
    void attributeIs(const string& name, const string& v);

private:
    Ptr<ManagerImpl> manager_;
    Ptr<Statistics> stats_;
    string name_;

};

string StatisticsRep::attribute(const string& name) {
    if(name == "expedite percentage") {
        return doubleToString(stats_->percentSegmentsWithExpeditedShipping());
    } else {
        // Assuming that name is name of an entity type
        int numEntities = stats_->entities(name);
        return intToString(numEntities);
    }
}

void StatisticsRep::attributeIs(const string& name, const string& v) {
}

string StatisticsRep::name() {
    return name_;
}

class ConnectivityRep : public Instance {
public:

    ConnectivityRep(const string& name, ManagerImpl* manager) :
        Instance(name), manager_(manager)
    {
        manager_ = manager;
        conn_ = new Connectivity(manager_->engine());
    }

    // Instance method
    string attribute(const string& name);

    // Instance method
    void attributeIs(const string& name, const string& v);

private:
    Ptr<ManagerImpl> manager_;
    Ptr<Connectivity> conn_;
    string name_;

};

string ConnectivityRep::attribute(const string& name) {
   int curIndex = 0;
   
   // Get the type of query
   int queryTypeEndIndex = name.find(" ", curIndex);
   string queryType = name.substr(0, queryTypeEndIndex);
   curIndex = queryTypeEndIndex + 1;
   
   // Get the location name that the query starts from
   int locationEndIndex = name.find(" : ", curIndex);
   string startLocation = name.substr(curIndex, locationEndIndex - curIndex);
   curIndex = locationEndIndex + strlen(" : ");
   
    if(!manager_->instance(startLocation) || !dynamic_cast<LocationRep* >(manager_->instance(startLocation).ptr())) {
        cerr << startLocation << " does not refer to a Location in the system " << endl;
        return "";
    }
   
   if(queryType == "explore") {
        Miles distance;
        Dollars cost;
        Hours time;
        Segment::ExpediteSupport expediteSupport;
        std::set<std::string> constraints;
    
        while(true) {
            // Keep looping through the string pulling out attribute names and values for those attributes
            
            int nextAttributeEndIndex = name.find(" ", curIndex);
            string nextAttribute = name.substr(curIndex, nextAttributeEndIndex - curIndex);
            curIndex = nextAttributeEndIndex + 1;
            int nextValEndIndex = name.find(" ", curIndex);
            
            string nextValue;
            if(nextValEndIndex == -1) {
                // If this is the last value, set curIndex to the sentintel so we know to leave
                nextValue = name.substr(curIndex);
                curIndex = -1;
            } else {
                nextValue = name.substr(curIndex, nextValEndIndex - curIndex);
                curIndex = nextValEndIndex + 1;
            }
        
            if(nextAttribute == "distance") {
                distance = Miles(atof(nextValue.c_str()));
                constraints.insert("distance");
            } else if(nextAttribute == "cost") {
                cost = Dollars(atof(nextValue.c_str()));
                constraints.insert("cost");
            } else if(nextAttribute == "time") {
                time = Hours(atof(nextValue.c_str()));
                constraints.insert("time");
            } else if(nextAttribute == "expedited") {
                if(nextValue == "no") {
                    expediteSupport = Segment::no();
                    constraints.insert("expedited");
                } else if(nextValue == "yes") {
                    expediteSupport = Segment::yes();
                    constraints.insert ("expedited");
                } else {
                    cerr << "Bad value " << nextValue << " for attribute " << nextAttribute << endl;
                    return "";
                }
            } else {
                cerr << "Invalid input string : " << name << endl;
                return "";
            }
        
            if(curIndex == -1) {
                break;
            }
        }
        

        std::vector<Path> paths;
        conn_->pathsWithConstraints(paths, startLocation, distance, cost, time, expediteSupport, constraints);
        
        string returnString = "";
        for(int i = 0; i < paths.size(); i++) {
            Path path = paths.at(i);
            vector<Ptr<Entity> >* steps = path.path();
            for(int j = 0; j < steps->size(); j++) {
                Ptr<Location> location = Ptr<Location>((Location*)steps->at(j).ptr());
                returnString += location->name();
                
                if(j < steps->size() - 1) {
                    Ptr<Segment> segment = Ptr<Segment>((Segment*)steps->at(++j).ptr());
                    returnString += "(" + segment->name() + ":" + location->name() + ":" + segment->name() + "') ";
                } else {
                    returnString += "\n";
                }
            }
            
            delete steps;
        }
        
        return returnString;
        
   } else if(queryType == "connect") {
        string endLocation = name.substr(curIndex);
        
        if(!manager_->instance(endLocation) || !dynamic_cast<LocationRep* >(manager_->instance(endLocation).ptr())) {
            cerr << endLocation << " does not refer to a Location in the system " << endl;
            return "";
        }
        
        std::vector<PathInfo>* paths = conn_->pathsBetweenLocations(startLocation, endLocation);
        
        string returnString = "";
        for(int i = 0; i < paths->size(); i++) {
            PathInfo path = paths->at(i);
            Dollars cost = path.cost;
            Hours time = path.time;
            Segment::ExpediteSupport expedite = path.expedited;
            
            returnString += doubleToString(cost.value()) + " ";
            returnString += doubleToString(time.value()) + " ";
            if(expedite == Segment::no()) {
                returnString += "no; ";
            } else {
                returnString += "yes; ";
            }
            
            vector<Ptr<Entity> >* steps = path.path.path();
            for(int j = 0; j < steps->size(); j++) {
                Ptr<Location> location = Ptr<Location>((Location*)steps->at(j).ptr());
                returnString += location->name();
                
                if(j < steps->size() - 1) {
                    Ptr<Segment> segment = Ptr<Segment>((Segment*)steps->at(++j).ptr());
                    returnString += "(" + segment->name() + ":" + location->name() + ":" + segment->name() + "') ";
                } else {
                    returnString += "\n";
                }
            }
        }
        
        delete paths;
        
        return returnString;
   }
   
   cerr << "Unsupported query " << queryType << endl;
   return "";
}

void ConnectivityRep::attributeIs(const string& name, const string& v) {
}

class FleetRep : public Instance {
public:

    FleetRep(const string& name, ManagerImpl* manager) :
        Instance(name), manager_(manager)
    {
        manager_ = manager;
        fleet_ = manager_->engine()->fleet();
    }

    // Instance method
    string attribute(const string& name);

    // Instance method
    void attributeIs(const string& name, const string& v);

private:
    Ptr<ManagerImpl> manager_;
    Ptr<Fleet> fleet_;

};

string FleetRep::attribute(const string& name) {

    int commaIndex = name.find(",");
    if(commaIndex != -1 && commaIndex < name.length() - 1) {
        Mode mode;
        
        // name is formatted as 'mode, property', so we need to split up the string
        string modeString = name.substr(0, commaIndex);
        string propertyString = name.substr(commaIndex + 2);
        
        if(modeString == "Truck") {
            mode = truck();
        } else if(modeString == "Boat") {
            mode = boat();
        } else if(modeString == "Plane") {
            mode = plane();
        } else {
            cerr << "Invalid mode given :" << modeString << endl;
            return "";
        }
        
        if(propertyString == "speed") {
            if(fleet_->speedInitialized(mode)) {
                MilesPerHour speed = fleet_->speed(mode);
                return doubleToString(speed.value());
            } else {
                return "";
            }
        } else if(propertyString == "cost") {
            if(fleet_->costInitialized(mode)) {
                Dollars cost = fleet_->cost(mode);
                return doubleToString(cost.value());
            } else {
                return "";
            }
        } else if(propertyString == "capacity") {
            if(fleet_->capacityInitialized(mode)) {
                Packages capacity = fleet_->capacity(mode);
                return intToString(capacity.value());
            } else {
                return "";
            }
        } else {
            cerr << "Invalid property given : " << propertyString << endl;
            return "";
        }  
    }
    return "";
}

void FleetRep::attributeIs(const string& name, const string& v) {
    // TODO
    int commaIndex = name.find(",");
    if(commaIndex != -1 && commaIndex < name.length() - 1) {
        Mode mode;
        
        // name is formatted as 'mode, property', so we need to split up the string
        string modeString = name.substr(0, commaIndex);
        string propertyString = name.substr(commaIndex + 2);
        
        if(modeString == "Truck") {
            mode = truck();
        } else if(modeString == "Boat") {
            mode = boat();
        } else if(modeString == "Plane") {
            mode = plane();
        } else {
            cerr << "Invalid mode given :" << modeString << endl;
        }
        
        if(propertyString == "speed") {
            double value = atof(v.c_str());
            if(value > 0) {
                fleet_->speedIs(mode, MilesPerHour(value));
            } else {
                cerr << "Invalid value of " << value << " for attribute " << propertyString << endl;
            }
        } else if(propertyString == "cost") {
            double value = atof(v.c_str());
            if(value >= 0) {           
                fleet_->costIs(mode, Dollars(value));
            } else {
                 cerr << "Invalid value of " << value << " for attribute " << propertyString << endl;
            }
        } else if(propertyString == "capacity") {
            int value = atoi(v.c_str());
            if(value > 0) {
                fleet_->capacityIs(mode, Packages(value));
            } else {
                 cerr << "Invalid value of " << value << " for attribute " << propertyString << endl;             
            }
        } else {
            cerr << "Invalid property given : " << propertyString << endl;
        }
    }
}

class ShippingManagerRep : public Instance {

public:
    ShippingManagerRep(const string& name, ManagerImpl* manager) :
        Instance(name), manager_(manager)
    {
        manager_ = manager;
        shippingManager_ = manager_->engine()->shippingManager();
    }

    // Instance method
    string attribute(const string& name);

    // Instance method
    void attributeIs(const string& name, const string& v);
    
    
private:
    Ptr<ManagerImpl> manager_;
    Ptr<ShippingManager> shippingManager_;
};

string ShippingManagerRep::attribute(const string& name) {
    if(name == "now") {
        return doubleToString(shippingManager_->now().value());
    } else {
        return "Invalid attribute name";
    }
}

void ShippingManagerRep::attributeIs(const string& name, const string& v) {
    if(name == "now") {
        double value = atof(v.c_str());
        
        if(value < shippingManager_->now().value()) {
            cerr << "Cannot move backwards in time. " << value << " is earlier than " << shippingManager_->now().value() << endl;
        } else {
            shippingManager_->nowIs(value);
        }
    } else {
        cerr << "Invalid attribute name " << name << endl;
    }
}

class RealTimeManagerRep : public Instance {

public:
    RealTimeManagerRep(const string& name, ManagerImpl* manager) :
        Instance(name), manager_(manager)
    {
        manager_ = manager;
        realTimeManager_ = manager_->engine()->realTimeManager();
    }
    
    // Instance method
    string attribute(const string& name);

    // Instance method
    void attributeIs(const string& name, const string& v);


private:
    Ptr<ManagerImpl> manager_;
    Ptr<RealTimeManager> realTimeManager_;
};


string RealTimeManagerRep::attribute(const string& name) {
    if(name == "realTime") {
        return doubleToString(realTimeManager_->realTime().value());
    }
}

void RealTimeManagerRep::attributeIs(const string& name, const string& v) {
    if(name == "realTime") {
        double value = atof(v.c_str());
        if(value > realTimeManager_->realTime().value()) {
            realTimeManager_->realTimeIs(value);
        }
    }
}

ManagerImpl::ManagerImpl() {
    engine_ = new Engine();
    stats_ = new StatisticsRep("stats", this);
    conn_  = new ConnectivityRep("conn", this);
    fleet_ = new FleetRep("fleet", this);
    //instance_["stats"] = stats_;
    //instance_["conn"] = conn_;
    //instance_["fleet"] = fleet_;
}

Ptr<Instance> ManagerImpl::instanceNew(const string& name, const string& type) {

    Ptr<Instance> t = NULL;
    
    if(!instance_[name]) { // This name is unique and has not been seen before
        if (type == "Truck terminal" || type == "Boat terminal" || type == "Plane terminal") {
            t = new TerminalRep(name, type, this);
        } else if(type == "Port") {
            t = new PortRep(name, this);
        } else if(type == "Customer") {
           t = new CustomerRep(name, this);
        } else if(type == "Truck segment" || type == "Boat segment" || type == "Plane segment") {
            t = new SegmentRep(name, type, this);
        } else if(type == "Stats") {
            t = stats_;
        } else if(type == "Conn") {
            t = conn_;
        } else if(type == "Fleet") {
            t = fleet_;
        } else if(type=="ShippingManager") {
            t = new ShippingManagerRep(name, this);
        } else if(type=="RealTimeManager") {
            t = new RealTimeManagerRep(name, this);
        }
    } else {
        cerr << "Instance with name " << name << " already exists" << endl;
        return NULL;
    }
        
    if(t) {
        instance_[name] = t;
    }
    
    return t;
}

Ptr<Instance> ManagerImpl::instance(const string& name) {
    map<string,Ptr<Instance> >::const_iterator t = instance_.find(name);

    return t == instance_.end() ? NULL : (*t).second;
}

void ManagerImpl::instanceDel(const string& name) {
    map<string,Ptr<Instance> >::const_iterator t = instance_.find(name);
    if(t != instance_.end()) {
        engine_->instanceDel(name);
        instance_.erase(name);
    }
}

Ptr<Engine> ManagerImpl::engine() {
    return engine_;
}

}

/*
 * This is the entry point for your library.
 * The client program will call this function to get a handle
 * on the Instance::Manager object, and from there will use
 * that object to interact with the middle layer (which will
 * in turn interact with the engine layer).
 */
Ptr<Instance::Manager> shippingInstanceManager() {
    return new Shipping::ManagerImpl();
}
