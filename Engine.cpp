#include "Engine.h"
#include <stack>
#include <deque>
#include <set>
#include <algorithm>

#include <string>
#include <stdlib.h>
#include <cstdio>
#include <iostream>

using namespace std;

string doubleToString(double value) {
    char buffer[256];
    snprintf(buffer, sizeof(buffer), "%.2f", value);
    return string(buffer);
}

string intToString(int value) {
    std::ostringstream s;
    s << value;
    return s.str();
}

namespace Shipping {
// Location

vector<Ptr<Entity> > *Path::path() {
    return path_;
}

void Path::pathIs(vector<Ptr<Entity> > *path) {
    path_ = path;
}

void Path::pathCopy(Path& copy) {
    vector<Ptr<Entity> > *newPathVec = new vector<Ptr<Entity> >;
    for (int i = 0; i < path_->size(); ++i) {
        newPathVec->push_back((*path_)[i]);
    }
    copy.pathIs(newPathVec);
}


void ShipmentActivity::numPackagesIs(int n) {
    numPackages_ = n;
}

int ShipmentActivity::numPackages() {
    return numPackages_;
}
    
void ShipmentActivity::finalDestIs(Fwk::Ptr<Location> dest) {
    finalDest_ = dest;
}

Fwk::Ptr<Location> ShipmentActivity::finalDest() {
    return finalDest_;
}

void ShipmentActivity::curDestIs(Fwk::Ptr<Location> dest) {
    curDest_ = dest;
}

Fwk::Ptr<Location> ShipmentActivity::curDest() {
    return curDest_;
}

Time ShipmentActivity::startTime() {
    return startTime_;
}

Fwk::Ptr<Segment> ShipmentActivity::curSegment() {
    if (path_.path()->size() == 0) {
        return NULL;
    }
    Fwk::Ptr<Segment> curSegment = Fwk::Ptr<Segment>(
        dynamic_cast<Segment *>(path_.path()->front().ptr())
    );
    return curSegment;
}

Fwk::Ptr<Segment> ShipmentActivity::nextSegment() {
    if (path_.path()->size() < 2) {
        return NULL;
    }
    Fwk::Ptr<Segment> nextSegment = Fwk::Ptr<Segment>(
        dynamic_cast<Segment *>(path_.path()->at(1).ptr())
    );
    return nextSegment;
}

void ShipmentActivity::atNextSegment() {
    Fwk::Ptr<Entity> curEntity = path_.path()->front();
    Fwk::Ptr<Segment> curSegmentCheck = Fwk::Ptr<Segment>(
        dynamic_cast<Segment *>(curEntity.ptr())
    );
   
    // logic: if the current entity at the front of the path
    // is a location, we pop one entity off the front to
    // get the next segment. If it's a segment, we pop two
    // off to get the next segment
    if (path_.path()->size() > 0) {
        path_.path()->erase(path_.path()->begin());
    }
    if (curSegmentCheck && path_.path()->size() > 0) {
        path_.path()->erase(path_.path()->begin());
    }
}

void ShipmentActivity::atNextLocation() {
    Fwk::Ptr<Entity> curEntity = path_.path()->front();
    Fwk::Ptr<Location> curLocationCheck = Fwk::Ptr<Location>(
        dynamic_cast<Location *>(curEntity.ptr())
    );
   
    // logic: if the current entity at the front of the path
    // is a location, we pop one entity off the front to
    // get the next segment. If it's a segment, we pop two
    // off to get the next segment
    if (path_.path()->size() > 0) {
        path_.path()->erase(path_.path()->begin());
    }
    if (curLocationCheck && path_.path()->size() > 0) {
        path_.path()->erase(path_.path()->begin());
    }
}

bool ShipmentActivity::atDest() {
    // we'll never pop the last location from the path,
    // so the path size will be 1 when it's at it's
    // destination
    return path_.path()->size() == 1;
}

void ShipmentActivity::sourceIs(Fwk::Ptr<Customer> source) {
    source_ = source;
}

Fwk::Ptr<Customer> ShipmentActivity::source() {
    return source_;
}

void CustomerReactor::checkAllSet() {
    if(rateSet_ && sizeSet_ && destSet_) {
        cout << "All Set" << endl;
        Fwk::Ptr<ShipmentInjectActivity> inject = new ShipmentInjectActivity(customer_->name(), manager_);
        Fwk::Ptr<InjectActivityReactor> injector = new InjectActivityReactor(manager_, inject.ptr());
        injector->sourceIs(customer_);
        customer_->injectorIs(injector);
    }
}


void CustomerReactor::onRateSet() {
    rateSet_ = true;
    cout << "Set rate in Reactor" << endl;
    checkAllSet();
}

void CustomerReactor::onSizeSet() {
    sizeSet_ = true;
    cout << "Set size in Reactor" << endl;
    checkAllSet();
}

void CustomerReactor::onDestSet() {
    destSet_ = true;
    cout << "Set dest in Reactor" << endl;
    checkAllSet();
}

void ShippingManager::engineIs(Fwk::Ptr<Engine> e) {
    engine_ = e;
}

Fwk::Ptr<Engine> ShippingManager::engine() {
    return engine_;
}

void ShippingManager::nowIs(Time t) {
    cout << "Now is " << t.value() << endl;
   while (!scheduledActivities_.empty()) {
        Activity::Ptr nextToRun = scheduledActivities_.top();
        
        if (nextToRun->nextTime() > t) {
            break;
        }
    
        cout << "Popping activity for time " << nextToRun->nextTime().value() << endl;
        now_ = nextToRun->nextTime();
        
        scheduledActivities_.pop();
        Fwk::Ptr<ShipmentActivity> nextShipment = Fwk::Ptr<ShipmentActivity>(
            dynamic_cast<ShipmentActivity *>(nextToRun.ptr())
        );
        if (nextShipment) {
            cout << "Shipment from " << nextShipment->source()->name() << " arrived at next dest " << nextShipment->curDest() << endl;
            shipmentActivityArrived(nextShipment);
        } else { 
            // inject activities
            Fwk::Ptr<ShipmentInjectActivity> injectActivity = Fwk::Ptr<ShipmentInjectActivity>(dynamic_cast<ShipmentInjectActivity *>(nextToRun.ptr()));
            injectActivity->statusIs(Activity::free);
        }
        
        Time diff = Time(nextToRun->nextTime().value() - now_.value());
        //usleep(( ((int)diff.value()) * 100000));
   }
   now_ = t;
}

void ShippingManager::shipmentActivityArrived(Fwk::Ptr<ShipmentActivity> shipment) {
    if (shipment->curSegment()) {
        engine_->activityIs(shipment->curSegment()->name(), NULL);
        engine_->numShipmentsIs(shipment->curSegment()->name(), engine_->numShipments(shipment->curSegment()->name()) - 1);
        shipment->atNextLocation();
        // activity finished
        Hours shippingLatency = Hours(now_.value() - shipment->startTime().value());
        shipment->finalDest()->onArrival(shippingLatency, shipment->pathCost());
        shipment->statusIs(Activity::executing);
        shipment->statusIs(Activity::free);
        cout << "Arrived " << endl;
    }

    if (!shipment->atDest()) { // doesn't do anything if we've reached end location
        double speed = engine_->fleet()->speed(shipment->nextSegment()->mode()).value();
        Time nextSegmentTravelTime = Time(shipment->nextSegment()->length().value() / speed);
        //Fwk::Ptr<Activity> nextSegmentCurActivity = NULL;
        //    engine_->activity(shipment->nextSegment()->name());
        int numShipmentsOnSegment = engine_->numShipments(shipment->nextSegment()->name());
        if (numShipmentsOnSegment == shipment->nextSegment()->capacity()) {
            cout << " Next segment is full. Waiting " << endl;
            //shipment->nextTimeIs(nextSegmentCurActivity->nextTime());
            shipment->nextTimeIs(Time(now_.value() + nextSegmentTravelTime.value()));
            // activity waiting
            shipment->statusIs(Activity::waiting);
            scheduledActivities_.push(shipment);
        } else {
            int capacity = engine_->fleet()->capacity(shipment->nextSegment()->mode()).value();
            if (shipment->numPackages() > capacity) {
                // TODO: should we change the shipment name?
                string shipmentName = shipment->name();
                Fwk::Ptr<ShipmentActivity> splitShipment = 
                    new ShipmentActivity(shipmentName, this, shipment->startTime());
                splitShipment->numPackagesIs(shipment->numPackages() - capacity);
                shipment->numPackagesIs(capacity);

                Path splitShipmentPath = splitShipment->path();
                splitShipmentPath.pathIs(new vector<Fwk::Ptr<Entity> >);
                shipment->path().pathCopy(splitShipmentPath);
                splitShipment->pathIs(splitShipmentPath);
                splitShipment->sourceIs(shipment->source());

                splitShipment->pathCostIs(shipment->pathCost());
                splitShipment->nextTimeIs(Time(now_.value() + nextSegmentTravelTime.value()));
                // activity waiting
                splitShipment->statusIs(Activity::waiting);
                scheduledActivities_.push(splitShipment);
            }
            
            cout << "Moving to next segment " << endl;
            shipment->atNextSegment();
            shipment->curSegment()->onShipment(shipment);

            //engine_->activityIs(shipment->curSegment()->name(), shipment);
            engine_->numShipmentsIs(shipment->curSegment()->name(), engine_->numShipments(shipment->curSegment()->name()) + 1);
            shipment->nextTimeIs(Time(now_.value() + nextSegmentTravelTime.value()));
            scheduledActivities_.push(shipment);
        }
    } else {
        cout << "Shipment from " << shipment->source()->name() << "arrived at final dest " << shipment->finalDest()->name() << endl;
    }
}

void RealTimeManager::realTimeIs(Time t) {
    double diff = t.value() - time_.value();
    for(int i = 1; i < (int)diff; i++) {
        shippingManager_->nowIs(time_.value() + i);
        usleep(1000000);
    }
}

Time RealTimeManager::realTime() {
    return time_;
}

void RealTimeManager::shippingManagerIs(Ptr<ShippingManager> shippingManager) {
    shippingManager_ = shippingManager;
}

void InjectActivityReactor::sourceIs(Fwk::Ptr<Customer> source) {
    source_ = source;
    Fwk::Ptr<Customer> destination = source_->dest();
    ShippingManager* shippingManager = dynamic_cast<ShippingManager*>(manager_.ptr());
    path_ = shippingManager->engine()->pathfinder()->path(source_, destination, false);
        std::vector<Fwk::Ptr<Entity> > * pathVec = path_.path();
        cout << "-------------" << endl;
        //cout << "-- " << source_->name() << " -- " << endl;
        for(int i = 0; i < pathVec->size(); i++) {
            Fwk::Ptr<Location> curLoc = dynamic_cast<Location*>(pathVec->at(i).ptr());
            if(curLoc) {
                cout << curLoc->name() << " - ";
            }
        }
        cout << endl;
        cout << "============" << endl;
    pathCost_ = shippingManager->engine()->pathCost(path_);
    activity_->nextTimeIs(Time(shippingManager->now().value() + (24.0 / source_->rate())));
    activity_->statusIs(Activity::nextTimeScheduled);
    //shippingManager->lastActivityIs(activity_);
    
}

Fwk::Ptr<Customer> InjectActivityReactor::source() {
    return source_;
}

void InjectActivityReactor::onStatus() {
    ShippingManager* shippingManager = dynamic_cast<ShippingManager*>(manager_.ptr());
    Fwk::Ptr<ShipmentActivity> shipment;
    
    switch(activity_->status()) {
        case Activity::free:
            // Insert new package here!!!
            shipment = new ShipmentActivity(string(""), Fwk::Ptr<ShippingManager>(shippingManager), shippingManager->now());
           shipment->numPackagesIs(source_->size());
           shipment->pathIs(path_);
           shipment->sourceIs(source_);
           shipment->finalDestIs(source_->dest());
           shipment->pathCostIs(pathCost_);
           shipment->nextTimeIs(shippingManager->now());
           shippingManager->lastActivityIs(shipment);
           
           cout << "Inserting new shipment from " <<  source_->name() << endl;
           cout << "Pop time is " << doubleToString(shipment->nextTime().value()) << endl;
            activity_->nextTimeIs(Time(shippingManager->now().value() + (24.0 / source_->rate())));
            activity_->statusIs(Activity::nextTimeScheduled);  
            break;
        
        case Activity::nextTimeScheduled:
            shippingManager->lastActivityIs(activity_);
            break;
        
        default:
            break;
        
    }
}

void InjectActivityReactor::onNextTime() {
}

Location::Location() {
    numShipmentsRecieved_ = 0;
    totalLatency_ = Hours(0.0);
    totalCost_ = Dollars(0.0);
}
    
void Location::nameIs(const string name) {
    name_ = name;
}
    
string Location::name() {
    return name_;
}
    
std::vector<Ptr<Segment> >* Location::segments() {
    return &segments_;
}
    
Ptr<Segment> Location::segment(const unsigned int segmentNumber) {
    int index = segmentNumber - 1;
    if(index >= 0 && index < segments_.size()) {
        return segments_[index];
    } else {
        return NULL;
    }
}
    
void Location::segmentIs(Ptr<Segment> segment) {
    if(segment) {
        segments_.push_back(segment);
    }
}

void Location::segmentDel(Ptr<Segment> segment) {
    for(int i = 0; i < segments_.size(); i++) {
        if(segments_[i] == segment) {
            segments_.erase(segments_.begin() + i);
            break;
        }
    }
}

void Location::onArrival(Hours latency, Dollars cost) {
    ++numShipmentsRecieved_;
    totalLatency_ = Hours(totalLatency_.value() + latency.value());
    totalCost_ = Dollars(totalCost_.value() + cost.value());
}

// Customer

void Customer::rateIs(double rate) {
    if(rate > 0) {
        rate_ = rate;
        cout << "Set rate" << endl;
        if(notifiee_) {
            notifiee_->onRateSet();
        }
    }
}

double Customer::rate() {
    return rate_;
}

void Customer::destIs(Ptr<Customer> dest) {
    dest_ = dest;
    cout << "Set dest" << endl;
    if(notifiee_) {
        notifiee_->onDestSet();
    }
}

Ptr<Customer> Customer::dest() {
    return dest_;
}

void Customer::sizeIs(int size) {
    if(size > 0) {
        size_ = size;
        cout << "Set size" << endl;
        if(notifiee_) {
            notifiee_->onSizeSet();
        }
    }
}

double Customer::size() {
    return size_;
}

void Customer::lastNotifieeIs(Ptr<Notifiee> n) {
    notifiee_ = n;
}

Ptr<Customer::Notifiee> Customer::notifiee() {
    return notifiee_;
}

Customer::Customer() {
    
}

void Customer::injectorIs(Ptr<InjectActivityReactor> injector) {
    injector_ = injector;
}

Ptr<InjectActivityReactor> Customer::injector() {
    return injector_;
}

// Segment

Segment::Segment() {
    capacity_ = 10;
    numShipmentsRecieved_ = 0;
}

void Segment::nameIs(const string name) {
    name_ = name;
}

string Segment::name() {
    return name_;
}
    
void Segment::modeIs(Mode mode) {
    mode_ = mode;
}

Mode Segment::mode() {
    return mode_;
}
    
void Segment::sourceIs(Ptr<Location> source) {
    source_ = source;
    Ptr<Segment> thisSegment = Ptr<Segment>(this);
    if(source_) {
        bool thisSegmentInSourceList = false;
        for (int i = 0; i < source->segments()->size(); ++i) {
            if (source->segments()->at(i) == thisSegment) {
                thisSegmentInSourceList = true;
            }
        }
        if (!thisSegmentInSourceList) {
            source->segmentIs(thisSegment);
        }
    }
}

Ptr<Location> Segment::source() {
    return source_;
}
    
void Segment::lengthIs(Miles length) {
    length_ = length;
    lengthInit_ = true;
}

Miles Segment::length() {
    return length_;
}

bool Segment::lengthInitialized() {
    return lengthInit_;
}
    
void Segment::returnSegmentIs(Ptr<Segment> returnSegment) {
    returnSegment_ = returnSegment;
}

Ptr<Segment> Segment::returnSegment() {
    return returnSegment_;
}
    
void Segment::difficultyIs(Difficulty difficulty) {
    difficulty_ = difficulty;
    difficultyInit_ = true;
}

Difficulty Segment::difficulty() {
    return difficulty_;
}

bool Segment::difficultyInitialized() {
    return difficultyInit_;
}
    
void Segment::expediteSupportIs(Segment::ExpediteSupport expediteSupport) {
    expediteSupport_ = expediteSupport;
    expediteInit_ = true;
}

Segment::ExpediteSupport Segment::expediteSupport() {
    return expediteSupport_;
}

bool Segment::expediteSupportInitialized() {
    return expediteInit_;
}

void Segment::onShipment(Ptr<ShipmentActivity> shipment) {
    ++numShipmentsRecieved_;
}

int Segment::numShipmentsRecieved() {
    return numShipmentsRecieved_;
}

void Segment::capacityIs(int capacity) {
    capacity_ = capacity;
}

int Segment::capacity() {
    return capacity_;
}

void Terminal::modeIs(Mode mode) {
    mode_ = mode;
}

Mode Terminal::mode() {
    return mode_;
}

Ptr<Entity> Engine::instanceNew(string name, string type) {
    Ptr<Entity> newEntity;
    if (type == "Truck terminal") {
        Ptr<Terminal> terminal = new Terminal;
        terminal->nameIs(name);
        terminal->modeIs(truck_);
        locationRecord_[name] = terminal;
        newEntity = terminal;
    } else if (type == "Boat terminal") {
        Ptr<Terminal> terminal = new Terminal;
        terminal->nameIs(name);
        terminal->modeIs(boat_);
        locationRecord_[name] = terminal;
        newEntity = terminal;
    } else if (type == "Plane terminal") {
        Ptr<Terminal> terminal = new Terminal;
        terminal->nameIs(name);
        terminal->modeIs(plane_);
        locationRecord_[name] = terminal;
        newEntity = terminal;
    } else if (type == "Port") {
        Ptr<Port> port = new Port;
        port->nameIs(name);
        locationRecord_[name] = port;
        newEntity = port;
    } else if (type == "Customer") {
        Ptr<Customer> customer = new Customer;
        customer->nameIs(name);
        locationRecord_[name] = customer;
        newEntity = customer;
        CustomerReactor* customerReactor = new CustomerReactor(shippingManager_, customer.ptr());
    } else if (type == "Truck segment") {
        Ptr<Segment> segment = new Segment();
        segment->nameIs(name);
        segment->modeIs(truck_);
        segment->sourceIs(NULL);
        segment->returnSegmentIs(NULL);
        segmentRecord_[name] = segment;
        newEntity = segment;
    } else if (type == "Boat segment") {
        Ptr<Segment> segment = new Segment();
        segment->nameIs(name);
        segment->modeIs(boat_);
        segment->sourceIs(NULL);
        segment->returnSegmentIs(NULL);
        segmentRecord_[name] = segment;
        newEntity = segment;
    } else { // if (type == "Plane segment") {
        Ptr<Segment> segment = new Segment();
        segment->nameIs(name);
        segment->modeIs(plane_);
        segment->sourceIs(NULL);
        segment->returnSegmentIs(NULL);
        segmentRecord_[name] = segment;
        newEntity = segment;
    }
    
    cout << "Engine now has " << segments().size() << " segments " << endl;
    statNotifiee_->onInstanceNew(newEntity, name, type);
    return newEntity;
}

void Engine::instanceDel(string name) {
    map<string, Ptr<Location> >::iterator locIt = locationRecord_.find(name);
    if (locIt != locationRecord_.end()) {
        Ptr<Location> toDelete = locIt->second;
        for (int i = 0; i < toDelete->segments()->size(); ++i) {
            Ptr<Segment> nextSegment = (*toDelete->segments())[i];
            nextSegment->sourceIs(NULL);
        }
        
        if (typeid(*(toDelete.ptr())) == typeid(Port)) {
            statNotifiee_->onInstanceDel(toDelete, name, "port");
        } else if (typeid(*(toDelete.ptr())) == typeid(Customer)) {
            statNotifiee_->onInstanceDel(toDelete, name, "customer");
        } else if (typeid(*(toDelete.ptr())) == typeid(Terminal)) {
            Ptr<Terminal> terminalToDelete = Ptr<Terminal>(
                dynamic_cast<Terminal *>(toDelete.ptr())
            );
            if (terminalToDelete->mode() == truck_) {
                statNotifiee_->onInstanceDel(toDelete, name, "Truck terminal");
            } else if (terminalToDelete->mode() == boat_) {
                statNotifiee_->onInstanceDel(toDelete, name, "Boat terminal");
            } else { // if (terminalToDelete->mode() == plane_) {
                statNotifiee_->onInstanceDel(toDelete, name, "Plane terminal");
            }
        } else {
            cerr << "Unrecognized location type" << endl;
        }
        
        locationRecord_.erase(locIt);
        return;
    }
    
    map<string, Ptr<Segment> >::iterator segIt = segmentRecord_.find(name);
    if (segIt != segmentRecord_.end()) {
        Ptr<Segment> toDelete = segIt->second;
        Ptr<Location> segSource = toDelete->source();
        
        if(segSource) {
            vector<Ptr<Segment> >::iterator sourceSegIt;
            sourceSegIt = find(segSource->segments()->begin(), 
                segSource->segments()->end(), toDelete
            );
            if (sourceSegIt != segSource->segments()->end()) {
                segSource->segments()->erase(sourceSegIt);
            }
        }
        if (toDelete->mode() == truck_) {
            statNotifiee_->onInstanceDel(toDelete, name, "Truck segment");
        } else if (toDelete->mode() == boat_) {
            statNotifiee_->onInstanceDel(toDelete, name, "Boat segment");
        } else { // if (toDelete->mode() == plane_) {
            statNotifiee_->onInstanceDel(toDelete, name, "Plane segment");
        }
        statNotifiee_->onInstanceDel(toDelete, name, "segment");
        segmentRecord_.erase(segIt);
        return;
    }
}

Ptr<Location> Engine::location(string name) {
     map<string, Ptr<Location> >::iterator it = locationRecord_.find(name);
     if (it == locationRecord_.end()) {
          return NULL;
     }
     return locationRecord_[name];
}

Ptr<Segment> Engine::segment(string name) {
     map<string, Ptr<Segment> >::iterator it = segmentRecord_.find(name);
     if (it == segmentRecord_.end()) {
          return NULL;
     }
     return segmentRecord_[name];
}

Ptr<Activity> Engine::activity(string segmentName) {
     map<string, Ptr<Activity> >::iterator it = activeSegments_.find(segmentName);
     if (it == activeSegments_.end()) {
         return NULL;
     }
     return activeSegments_[segmentName];
}

void Engine::activityIs(string segmentName, Ptr<Activity> activity) {
    activeSegments_[segmentName] = activity;
}

int Engine::numShipments(string segment) {
     map<string, int>::iterator it = numShipments_.find(segment);
     if (it == numShipments_.end()) {
         numShipments_[segment] = 0;
     }
     return numShipments_[segment];
}

void Engine::numShipmentsIs(string segment, int num) {
    numShipments_[segment] = num;
}

Dollars Engine::pathCost(Path& path) {
    double runningCost = 0.0;
    for (int i = 0; i < path.path()->size(); ++i) {
        if (i %2 == 0) {
            continue;
        }
        Ptr<Segment> segment = Ptr<Segment>(
            dynamic_cast<Segment *>(path.path()->at(i).ptr())
        );
        if (!segment) {
            continue;
        }
        double costPerMile = fleet_->cost(segment->mode()).value();
        runningCost += segment->length().value() * costPerMile;
    }
    return Dollars(runningCost);
}

vector<Ptr<Location> > Engine::locations() {
    vector<Ptr<Location> > locations;
    map<string, Ptr<Location> >::iterator it;
    map<string, Ptr<Location> >::iterator itEnd = locationRecord_.end();
    for (it = locationRecord_.begin(); it != itEnd; ++it) {
        locations.push_back(it->second);
    }
    return locations;
}

vector<Ptr<Segment> > Engine::segments() {
    vector<Ptr<Segment> > segments;
    map<string, Ptr<Segment> >::iterator it;
    map<string, Ptr<Segment> >::iterator itEnd = segmentRecord_.end();
    for (it = segmentRecord_.begin(); it != itEnd; ++it) {
        segments.push_back(it->second);
    }
    return segments;
}

Engine::StatNotifiee::StatNotifiee(Ptr<Engine> e) {
    e->statNotifieeIs(this);
}

Fwk:: Ptr<Pathfinder> Engine::pathfinder() {
    if(pathFinder_) {
        return pathFinder_;
    } else {
        pathFinder_ = new Pathfinder(this);
        return pathFinder_;
    }
}

Fwk::Ptr<ShippingManager> Engine::shippingManager() {
    return shippingManager_;
}

Fwk::Ptr<RealTimeManager> Engine::realTimeManager() {
    return realTimeManager_;
}

Statistics::Statistics(Ptr<Engine> engine) {
    engine_ = engine;
}

int Statistics::entities(string type) {
    return typeCountMap_[type];
}

double Statistics::percentSegmentsWithExpeditedShipping() {
    int numSegments = 0;
    int numSegmentsWithExpeditedShipping = 0;
    for (int i = 0; i < segmentList_.size(); ++i) {
        string segmentName = segmentList_[i];
        Ptr<Segment> segment = engine_->segment(segmentName);
        if (segment->expediteSupport() == Segment::yes()) {
            ++numSegmentsWithExpeditedShipping;
        }
        ++numSegments;
    }
    if (numSegments == 0) {
        return 0.0;
    }
    return 100.0 * (double) numSegmentsWithExpeditedShipping / (double) numSegments;
}

void Statistics::onInstanceNew(Ptr<Entity> entity, string name, string type) {
    map<string,int >::iterator it = typeCountMap_.find(type);
    if (it == typeCountMap_.end()) {
        typeCountMap_[type] = 1;
    } else {
        ++typeCountMap_[type];
    }
    if (type == "Truck segment" || type == "Boat segment" || type == "Plane segment") {
        segmentList_.push_back(name);
    }
}

void Statistics::onInstanceDel(Ptr<Entity> entity, string name, string type) {
    --typeCountMap_[type];
    if (type == "Truck segment" || type == "Boat segment" || type == "Plane segment") {
        vector<string>::iterator it = find(segmentList_.begin(), segmentList_.end(), name);
        if (it != segmentList_.end()) {
            segmentList_.erase(it);
        }
    }
}

Connectivity::Connectivity(Ptr<Engine> engine) {
    engine_ = engine;
}

void Connectivity::pathsWithConstraints(
    vector<Path>& allPaths,
    string sourceName, 
    Miles distance, 
    Dollars cost, 
    Hours time, 
    Segment::ExpediteSupport expedited,
    set<string> constraints
) {
    Ptr<Location> source = engine_->location(sourceName);
    Miles curDistance = Miles(0);
    Dollars curCost = Dollars(0);
    Hours curTime = Hours(0);

    Path startPath;
    vector<Ptr<Entity> > startPathVec;
    startPath.pathIs(&startPathVec);
    startPathVec.push_back(source);

    vector<Ptr<Segment> > *connSegments = source->segments();
    for (int i = 0; i < connSegments->size(); ++i) {
        Ptr<Segment> nextSegment = (*connSegments)[i];
        if (constraints.find("expedited") != constraints.end()  
                && expedited == Segment::yes() 
                && nextSegment->expediteSupport() == Segment::no()) {
            continue;
        }
        
        if(!nextSegment->returnSegment() || !nextSegment->returnSegment()->source()) {
            continue;
        }
        
        vector<Ptr<Entity> > *pathVec = startPath.path();
        pathVec->push_back(nextSegment);
        Ptr<Location> nextLocation = nextSegment->returnSegment()->source();
        pathVec->push_back(nextLocation);

        Dollars costPerMile = engine_->fleet()->cost(nextSegment->mode());
        Dollars nextCost = Dollars(costPerMile.value() * nextSegment->length().value());
        MilesPerHour mph = MilesPerHour(engine_->fleet()->speed(nextSegment->mode()));
        Hours nextTime = Hours(nextSegment->length().value() / mph.value());
        if (expedited == Segment::yes_) {
            nextCost = Dollars(curCost.value() + 1.5 * nextCost.value());
            nextTime = Hours(curTime.value() + 0.7 * nextTime.value());
        } else {
            nextCost = Dollars(curCost.value() + nextCost.value());
            nextTime = Hours(curTime.value() + nextTime.value());
        }

        pathsWithConstraintsRecursive(
            allPaths,
            startPath,
            source,
            nextLocation,
            distance,
            cost,
            time,
            expedited,
            constraints,
            Miles(curDistance.value() + nextSegment->length().value()),
            nextCost,
            nextTime
        );
        pathVec->pop_back();
        pathVec->pop_back();
    }

}

void Connectivity::pathsWithConstraintsRecursive(
    vector<Path>& allPaths,
    Path& path,
    Ptr<Location> source,
    Ptr<Location> current,
    Miles distance,
    Dollars cost,
    Hours time,
    Segment::ExpediteSupport expedited,
    set<string>& constraints,
    Miles curDistance,
    Dollars curCost,
    Hours curTime
) {
    if (current == source) {
        return;
    }
    if (constraints.find("distance") != constraints.end()
            && curDistance.value() > distance.value()) {
        return;
    }
    if (constraints.find("cost") != constraints.end()
            && curCost.value() > cost.value()) {
        return;
    }
    if (constraints.find("time") != constraints.end()
            && curTime.value() > time.value()) {
        return;
    }

    Path pathCopy;
    path.pathCopy(pathCopy);
    allPaths.push_back(pathCopy);

    vector<Ptr<Segment> > *connSegments = current->segments();
    for (int i = 0; i < connSegments->size(); ++i) {
        Ptr<Segment> nextSegment = (*connSegments)[i];
        if (constraints.find("expedited") != constraints.end()  
                && expedited == Segment::yes() 
                && nextSegment->expediteSupport() == Segment::no()) {
            continue;
        }
        
        if(!nextSegment->returnSegment() || !nextSegment->returnSegment()->source()) {
            continue;
        }
        
        vector<Ptr<Entity> > *pathVec = path.path();
        pathVec->push_back(nextSegment);
        Ptr<Location> nextLocation = nextSegment->returnSegment()->source();
        pathVec->push_back(nextLocation);

        double costPerMile = engine_->fleet()->cost(nextSegment->mode()).value();
        Dollars nextCost = Dollars(costPerMile * nextSegment->length().value());
        double mph = engine_->fleet()->speed(nextSegment->mode()).value();
        Hours nextTime = Hours (nextSegment->length().value() / mph);
        if (expedited == Segment::yes_) {
            nextCost = Dollars(curCost.value() + 1.5 * nextCost.value());
            nextTime = Hours(curTime.value() + 0.7 * nextTime.value());
        } else {
            nextCost = Dollars(curCost.value() + nextCost.value());
            nextTime = Hours(curTime.value() + nextTime.value());
        }
        pathsWithConstraintsRecursive(
            allPaths,
            path,
            source,
            nextLocation,
            distance,
            cost,
            time,
            expedited,
            constraints,
            Miles(curDistance.value() + nextSegment->length().value()),
            nextCost,
            nextTime
        );
        pathVec->pop_back();
        pathVec->pop_back();
    }
}

vector<PathInfo> *Connectivity::pathsBetweenLocations(
    string sourceName, 
    string destName
) {
    vector<Path> allPaths;
    Ptr<Location> source = engine_->location(sourceName);
    Ptr<Location> dest = engine_->location(destName);
    Ptr<Location> current = source;

    vector<Ptr<Entity> > startPathVec;
    Path startPath;
    startPath.pathIs(&startPathVec);

    vector<Ptr<Segment> > *connSegments = source->segments();
    startPathVec.push_back(source);
    for (int i = 0; i < connSegments->size(); ++i) {
        Ptr<Segment> nextSegment = (*connSegments)[i];
        if(!nextSegment->returnSegment() || !nextSegment->returnSegment()->source()) {
            continue;
        }
        Ptr<Location> nextLocation = nextSegment->returnSegment()->source();
        startPathVec.push_back(nextSegment);
        startPathVec.push_back(nextLocation);
        pathsBetweenLocationsRecursive(allPaths, startPath, source, nextLocation, dest);
        startPathVec.pop_back();
        startPathVec.pop_back();
    }
    return pathInfoForPaths(allPaths);
}

void Connectivity::pathsBetweenLocationsRecursive(
    vector<Path>& allPaths,
    Path& path,
    Ptr<Location> source,
    Ptr<Location> current,
    Ptr<Location> dest
) {
    if (current == source) {
        return;
    }
    if (current == dest) {
        Path pathCopy;
        path.pathCopy(pathCopy);
        allPaths.push_back(pathCopy);
        return;
    }
    vector<Ptr<Segment> > *connSegments = current->segments();
    for (int i = 0; i < connSegments->size(); ++i) {
        Ptr<Segment> nextSegment = (*connSegments)[i];
        if(!nextSegment->returnSegment() || !nextSegment->returnSegment()->source()) {
            continue;
        }
        Ptr<Location> nextLocation = nextSegment->returnSegment()->source();
        vector<Ptr<Entity> > *pathVec = path.path();
        pathVec->push_back(nextSegment);
        pathVec->push_back(nextLocation);
        pathsBetweenLocationsRecursive(allPaths, path, source, nextLocation, dest);
        pathVec->pop_back();
        pathVec->pop_back();
    }
}

vector<PathInfo> *Connectivity::pathInfoForPaths(vector<Path>& allPaths) {
    vector<PathInfo> *allPathInfo = new vector<PathInfo>;
    for (int i = 0; i < allPaths.size(); ++i) {
        Hours totalTime = Hours(0);
        Dollars totalCost = Dollars(0);
        Segment::ExpediteSupport notExpedited = Segment::no_;

        vector<Ptr<Entity> > *nextPathVec = allPaths[i].path();
        bool canBeExpedited = true;
        for (int j = 0; j < nextPathVec->size(); ++j) {
            if (j % 2 == 1) { // gets segments only, not locations
                Ptr<Segment> nextSegment = Ptr<Segment>(
                    dynamic_cast<Segment *>(((*nextPathVec)[j]).ptr())
                );
                if (nextSegment->expediteSupport() == Segment::no()) {
                    canBeExpedited = false;
               }
                MilesPerHour speed = engine_->fleet()->speed(nextSegment->mode());
                totalTime = Hours(
                    totalTime.value() + nextSegment->length().value() / speed.value()
                );
                Dollars cost = engine_->fleet()->cost(nextSegment->mode());
                totalCost = Dollars(
                    totalCost.value() + nextSegment->length().value() * cost.value()
                );
                PathInfo pi;
                pi.path = allPaths[i];
                pi.cost = totalCost;
                pi.time = totalTime;
                pi.expedited = notExpedited;
                allPathInfo->push_back(pi);
            }
        }
        if (canBeExpedited) {
            Dollars eTotalCost = Dollars(0);
            Hours eTotalTime = Hours(0);
            Segment::ExpediteSupport expedited = Segment::yes(); 
            for (int k = 0; k < nextPathVec->size(); ++k) {
                if (k % 2 == 1) {
                    Ptr<Segment> nextSegment = Ptr<Segment>(
                        dynamic_cast<Segment *>(((*nextPathVec)[k]).ptr())
                    );
                    MilesPerHour speed = engine_->fleet()->speed(nextSegment->mode());
                    eTotalTime = Hours(
                        eTotalTime.value() + 0.7 * nextSegment->length().value() / speed.value()
                    );
                    Dollars cost = engine_->fleet()->cost(nextSegment->mode());
                    eTotalCost = Dollars(
                        eTotalCost.value() + nextSegment->length().value() * cost.value() * 1.5
                    );
                }
            }
            PathInfo epi;
            Path copy;
            allPaths[i].pathCopy(copy);
            epi.path = copy;
            epi.cost = eTotalCost;
            epi.time = eTotalTime;
            epi.expedited = expedited;
            allPathInfo->push_back(epi);
        }
    }
    return allPathInfo;
}

Fleet::Fleet() {
    for(int i = truck_; i <= plane_; i++) {
        modeToSpeedMap_[static_cast<Mode>(i)] = 1.0;
        modeToCostMap_[static_cast<Mode>(i)] = 1.0;
        modeToCapacityMap_[static_cast<Mode>(i)] = 1;
        
        cout << modeToCapacityMap_[static_cast<Mode>(i)] << endl;
    }
}

void Fleet::speedIs(Mode mode, MilesPerHour speed) {
    modeToSpeedMap_[mode] = speed.value();
    modeSpeedInitialized_[mode] = true;
}

void Fleet::costIs(Mode mode, Dollars cost) {
    modeToCostMap_[mode] = cost.value();
    modeCostInitialized_[mode] = true;
}

void Fleet::capacityIs(Mode mode, Packages capacity) {
    modeToCapacityMap_[mode] = capacity.value();
    modeCapacityInitialized_[mode] = true;
}

bool Fleet::speedInitialized(Mode mode) {
    if(modeSpeedInitialized_[mode]) {
        return true;
    }
    return false;
}

bool Fleet::costInitialized(Mode mode) {
    if(modeCostInitialized_[mode]) {
        return true;
    }
    return false;
}

bool Fleet::capacityInitialized(Mode mode) {
    if(modeCapacityInitialized_[mode]) {
        return true;
    }
    return false;
}

MilesPerHour Fleet::speed(Mode mode) {
    return MilesPerHour(modeToSpeedMap_[mode]);
}


Dollars Fleet::cost(Mode mode) {
    return Dollars(modeToCostMap_[mode]);
}

Packages Fleet::capacity(Mode mode) {
    return Packages(modeToCapacityMap_[mode]);
}

Pathfinder::Pathfinder(Ptr<Engine> engine) {
    engine_ = engine;

    // construct minimum spanning tree
    map<string, set<string> > minCostForest;
    map<string, set<string> > minDistForest;

    vector<Ptr<Location> > locations = engine_->locations();
    for (int i = 0; i < locations.size(); ++i) {
        set<string> nextTree;
        nextTree.insert(locations[i]->name());
        minCostForest[locations[i]->name()] = nextTree;
        minDistForest[locations[i]->name()] = nextTree;
    }
    vector<Ptr<Segment> > segments = engine_->segments();
    
    vector<Ptr<Segment> > orderedMinCostSegments = insertionSort(segments, true);
    vector<Ptr<Segment> > orderedMinDistSegments = insertionSort(segments, false);

    for (int i = 0; i < orderedMinCostSegments.size(); ++i) {
        Ptr<Segment> nextSegment = orderedMinCostSegments[i];
        if (nextSegment->source()) {
            forestMerge(minCostForest, nextSegment, true);
        }
    }

    for (int i = 0; i < orderedMinDistSegments.size(); ++i) {
        Ptr<Segment> nextSegment = orderedMinDistSegments[i];
        if (nextSegment->source()) {
            forestMerge(minDistForest, nextSegment, false);
        }
    }

}

Path Pathfinder::path(Ptr<Location> source, Ptr<Location> dest, bool minCost) {
   set<string> visited;
   visited.insert(source->name());
   
   vector<Ptr<Entity> > *startPathVec = new vector<Ptr<Entity> >();
   startPathVec->push_back(source);
   Path startPath;
   startPath.pathIs(startPathVec);

   if (pathRecursive(startPath, dest, visited, minCost)) {
       return startPath;
   }

   // what do?
   return startPath;
}

bool Pathfinder::pathRecursive(
    Path& curPath, 
    Ptr<Location> dest, 
    set<string>& visited,
    bool minCost
) {
    vector<Ptr<Entity> > *curPathVec = curPath.path();
    Ptr<Location> pathEnd = Ptr<Location>(
        dynamic_cast<Location *>(curPathVec->at(curPathVec->size() - 1).ptr())
    );
    if (pathEnd == dest) {
        return true;
    }
    
    SpanningTreeLocation spLoc;
    if (minCost) {
        spLoc = minCostSpanningTree_[pathEnd->name()];
    } else {
        spLoc = minDistSpanningTree_[pathEnd->name()];
    }
    for (int i = 0; i < spLoc.spanningTreeSegments.size(); ++i) {
        Ptr<Segment> nextSegment = spLoc.spanningTreeSegments[i];
        if (nextSegment->returnSegment() && nextSegment->returnSegment()->source()) {      
            set<string>::iterator it = 
                visited.find(nextSegment->returnSegment()->source()->name()); 
            if (it != visited.end()) {
                continue;
            }
            curPathVec->push_back(nextSegment);
            curPathVec->push_back(nextSegment->returnSegment()->source());
            visited.insert(nextSegment->returnSegment()->source()->name());
            if (pathRecursive(curPath, dest, visited, minCost)) {
                return true;
            }
            curPathVec->pop_back();
            curPathVec->pop_back();
        }
    }
    return false;
}

void Pathfinder::forestMerge(
    map<string, set<string> >& forest, 
    Ptr<Segment> nextSegment, 
    bool minCost
) {
    string sourceName = nextSegment->source()->name();
    string destName = nextSegment->returnSegment()->source()->name();
    if (equal(
        forest[sourceName].begin(), forest[sourceName].end(), forest[destName].begin()
    )) {
        return;
    }
    if (minCost) {
        // create SpanningTreeLocations if necessary
        map<string, SpanningTreeLocation>::iterator sourceIt = 
            minCostSpanningTree_.find(sourceName);
        if (sourceIt == minCostSpanningTree_.end()) {
            Ptr<Location> source = engine_->location(sourceName);
            SpanningTreeLocation newSpanningTreeLocation;
            newSpanningTreeLocation.location = source;
            minCostSpanningTree_[sourceName] = newSpanningTreeLocation;
        }
        map<string, SpanningTreeLocation>::iterator destIt = 
            minCostSpanningTree_.find(destName);
        if (destIt == minCostSpanningTree_.end()) {
            Ptr<Location> dest = engine_->location(destName);
            SpanningTreeLocation newSpanningTreeLocation;
            newSpanningTreeLocation.location = dest;
            minCostSpanningTree_[destName] = newSpanningTreeLocation;
        }
        
        // merge trees
        SpanningTreeLocation sourceSpanningTree = minCostSpanningTree_[sourceName];
        SpanningTreeLocation destSpanningTree = minCostSpanningTree_[destName];
        sourceSpanningTree.spanningTreeSegments.push_back(nextSegment);
        destSpanningTree.spanningTreeSegments.push_back(nextSegment->returnSegment());
        minCostSpanningTree_[sourceName] = sourceSpanningTree;
        minCostSpanningTree_[destName] = destSpanningTree;
    } else {
        // create SpanningTreeLocations if necessary
        map<string, SpanningTreeLocation>::iterator sourceIt = 
            minDistSpanningTree_.find(sourceName);
        if (sourceIt == minDistSpanningTree_.end()) {
            Ptr<Location> source = engine_->location(sourceName);
            SpanningTreeLocation newSpanningTreeLocation;
            newSpanningTreeLocation.location = source;
            minDistSpanningTree_[sourceName] = newSpanningTreeLocation;
        }
        map<string, SpanningTreeLocation>::iterator destIt = 
            minDistSpanningTree_.find(destName);
        if (destIt == minDistSpanningTree_.end()) {
            Ptr<Location> dest = engine_->location(destName);
            SpanningTreeLocation newSpanningTreeLocation;
            newSpanningTreeLocation.location = dest;
            minDistSpanningTree_[destName] = newSpanningTreeLocation;
        }

        // merge trees
        SpanningTreeLocation sourceSpanningTree = minDistSpanningTree_[sourceName];
        SpanningTreeLocation destSpanningTree = minDistSpanningTree_[destName];
        sourceSpanningTree.spanningTreeSegments.push_back(nextSegment);
        destSpanningTree.spanningTreeSegments.push_back(nextSegment->returnSegment());
        minDistSpanningTree_[sourceName] = sourceSpanningTree;
        minDistSpanningTree_[destName] = destSpanningTree;
    }
    
    // merge tree sets for Kruskal's
    set<string> sourceTree = forest[sourceName];
    set<string> destTree = forest[destName];
    set<string> newTree(sourceTree);
    newTree.insert(destTree.begin(), destTree.end());
    forest[sourceName] = newTree;
    forest[destName] = newTree;
}

double Pathfinder::segmentCost(Ptr<Segment> segment, bool minCost) {
    if (minCost) {
        return segment->length().value() * engine_->fleet()->cost(segment->mode()).value();
    } else {
        return segment->length().value() / engine_->fleet()->speed(segment->mode()).value();
    }
}

vector<Ptr<Segment> > Pathfinder::insertionSort(
    vector<Ptr<Segment> >& unsorted, 
    bool minCost
) {
    vector<Ptr<Segment> > sorted;
    for (int i = 0; i < unsorted.size(); ++i) {
        Ptr<Segment> nextSegment = unsorted[i];
        double nextSegmentCost = segmentCost(nextSegment, minCost);
        vector<Ptr<Segment> >::iterator it;
        bool inserted = false;
        for (it = sorted.begin(); it != sorted.end(); ++it) {
            Ptr<Segment> sortedSegment = *it;
            if (nextSegmentCost < segmentCost(sortedSegment, minCost)) {
                sorted.insert(it, nextSegment);
                inserted = true;
                break;
            }
        }
        if (!inserted) {
            sorted.push_back(nextSegment);
        }
    }
    return sorted;
}

}
