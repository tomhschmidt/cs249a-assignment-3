#include <string>
#include <ostream>
#include <iostream>
#include <string>
#include "Instance.h"

using std::cout;
using std::cerr;
using std::endl;
using std::string;

int main(int argc, char *argv[]) {
    Ptr<Instance::Manager> manager = shippingInstanceManager();

    if (manager == NULL) {
        cerr << "Unexpected NULL manager." << endl;
        return 1;
    }

    Ptr<Instance> stats = manager->instanceNew("myStats", "Stats");

    if (stats == NULL) {
        cerr << "Unexpected NULL stats." << endl;
        return 1;
    }

    Ptr<Instance> fleet = manager->instanceNew("myFleet", "Fleet");

    if (fleet == NULL) {
        cerr << "Unexpected NULL." << endl;
        return 1;
    }  

    /* This tests instanceDel */
    Ptr<Instance> loc1 = manager->instanceNew("loc1", "Port");
    Ptr<Instance> loc2 = manager->instanceNew("loc2", "Port");
    Ptr<Instance> seg4 = manager->instanceNew("seg4", "Boat segment");
    Ptr<Instance> seg5 = manager->instanceNew("seg5", "Boat segment");
    
    seg4->attributeIs("source", "loc1");
    seg4->attributeIs("return segment", "seg5");
    seg5->attributeIs("source", "loc2");

    /* Delete location 2.  This should either a) make the source
       of seg5 equal to the empty string, or b) destroy seg5 */
    manager->instanceDel("loc2");

    return 0;
}
