#ifndef __ACTIVITY_H__
#define __ACTIVITY_H__

#include <string>
#include <queue>

#include "PtrInterface.h"
#include "Ptr.h"

#include "Nominal.h"
//#include "fwk/NamedInterface.h"
#include "Notifiee.h"

using std::string;

/* Define the type 'Time' */
class Time : public Ordinal<Time,double> {
public:
    Time(double time=0) : Ordinal<Time,double>(time)
    {}
};

class Activity : public Fwk::PtrInterface<Activity> {
 public:
    typedef Fwk::Ptr<Activity> Ptr;
    
    /* Notifiee class for Activities */
 class Notifiee : public Fwk::BaseNotifiee<Activity> {
    public:
	typedef Fwk::Ptr<Notifiee> Ptr;

        Notifiee(Activity* act) : Fwk::BaseNotifiee<Activity>(act) {}

        virtual void onNextTime() {}
	virtual void onStatus() {}
    };

    class Manager;

    enum Status {
        free, waiting, ready, executing, nextTimeScheduled, deleted
    };

    virtual Status status() const = 0;
    virtual void statusIs(Status s)  = 0;
    
    virtual Time nextTime() const = 0;
    virtual void nextTimeIs(Time t) = 0;

    virtual Fwk::Ptr<Notifiee> notifiee() const = 0;

    virtual void lastNotifieeIs(Notifiee* n) = 0;

    virtual string name() const { return name_; }

protected:
    Activity(const string &name)
        : name_(name)
    {}
    Activity::Notifiee* notifee_;

private:
    string name_;

};

extern Fwk::Ptr<Activity::Manager> activityManagerInstance();

class Activity::Manager : public Fwk::PtrInterface<Activity::Manager> {
public:

    class ActivityComp : public binary_function<Activity::Ptr, Activity::Ptr, bool> {
        public:
            ActivityComp() {}
    
            bool operator()(Activity::Ptr a, Activity::Ptr b) const {
                return (a->nextTime() > b->nextTime());
            }
        };

    typedef Fwk::Ptr<Activity::Manager> Ptr;

    virtual Fwk::Ptr<Activity> activityNew(const string &name) = 0;

    virtual Fwk::Ptr<Activity> activity(const string &name) const = 0;

    virtual void activityDel(const string &name) = 0;

    virtual void lastActivityIs(Activity::Ptr) = 0;

    virtual Time now() const = 0;
    virtual void nowIs(Time) = 0;


protected:
    priority_queue<Activity::Ptr, vector<Activity::Ptr>, ActivityComp> scheduledActivities_;
    map<string, Activity::Ptr> activities_; //pool of all activities
    Time now_;

    static Fwk::Ptr<Activity::Manager> activityInstance_;	

};

#endif
