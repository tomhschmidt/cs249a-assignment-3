/*
 * Sample client code for CS249A Assignment 3.
 *
 * This file contains a rough skeleton of what your simulation 
 * client code looks like. In particular, you might find it useful 
 * for writing your verification.cpp file. This file does not 
 * compile.
 *
 * Everything in this file is a sketch - you don't need to follow 
 * it at all, including the placement of statements and naming of 
 * attributes and variables. However, please document your design 
 * decisions in README. For example, if you decide to use a command 
 * line parameter to switch between real and virtual-time managers, 
 * please describe how to do that in your README.
 *
 * Another example is semantics of attribute mutators: you may 
 * decide when a mutator takes effect. For instance, setting a 
 * customer's transfer rate between two settings of the virtual time 
 * manager's "now" attribute (say, 50 and 100) could imply stopping 
 * shipment injections at virtual time 50, or at virtual time 100. 
 * Make sure you document your decision and take this into account 
 * when performing analyses.
 *
 */

#include <iostream>
#include <vector>
#include "Instance.h"
//#include "fwk/Exception.h"
#include "ActivityImpl.h"

using namespace std;
//using Shipping::Exception;

int main(int argc, char *argv[]) {

    Ptr<Instance::Manager> manager = shippingInstanceManager();
    
    Ptr<Instance> shippingManager = manager->instanceNew("shippingManager", "ShippingManager");

	/* Set up the network */
	vector< Ptr<Instance> > loc;
	vector< Ptr<Instance> > seg;
	
	string sourceBase = "source";
	string toSegBase = "AtoSeg";
	string fromSegBase = "AfromSeg";
	Ptr<Instance> destination = manager->instanceNew("destination", "Customer");
	
	for(int i = 0; i < 100; i++) {
        std::stringstream sstm;
        sstm << sourceBase << i;
        string result = sstm.str();
	    Ptr<Instance> curSource = manager->instanceNew(result, "Customer");
	    
	    std::stringstream toSegStream;
        toSegStream << toSegBase << i;
        string toSeg = toSegStream.str();
        cout << "toSeg : " << toSeg << endl;
	    Ptr<Instance> truckSeg1 = manager->instanceNew(toSeg, "Truck segment");
	    
	    std::stringstream fromSegStream;
        fromSegStream << fromSegBase << i;
        string fromSeg = fromSegStream.str();
        cout << "fromSeg : " << fromSeg << endl;
	    Ptr<Instance> truckSeg2 = manager->instanceNew(fromSeg, "Truck segment");
	    
	    truckSeg1->attributeIs("source", result);
	    truckSeg2->attributeIs("source", "destination");
	    truckSeg1->attributeIs("return segment", fromSeg);
	    truckSeg1->attributeIs("length", "10");
	    truckSeg2->attributeIs("length", "10");
	}
	
	Ptr<Instance> terminal = manager->instanceNew("terminal", "Truck terminal");
	
	Ptr<Instance> destToTerm = manager->instanceNew("destToTerm", "Truck segment");
	Ptr<Instance> termToDest = manager->instanceNew("termToDest", "Truck segment");
	destToTerm->attributeIs("source", "destination");
	termToDest->attributeIs("source", "terminal");
	destToTerm->attributeIs("return segment", "termToDest");
	
	string BtoSegBase = "BtoSeg";
	string BfromSegBase = "BfromSeg";
	string CtoSegBase = "CtoSegBase";
	string CfromSegBase = "CfromSegBase";
	string terminalBase = "terminal";
	
	string finalSourceBase = "finalSource";
	
	for(int i = 0; i < 10; i++) {
	    std::stringstream sstm;
        sstm << terminalBase << i;
        string result = sstm.str();
	    Ptr<Instance> curSource = manager->instanceNew(result, "Truck terminal");
	    
	    for(int j = 0; j < 10; j++) {
            std::stringstream sstm;
            sstm << finalSourceBase << i << j;
            string finalSource = sstm.str();
            Ptr<Instance> curSource = manager->instanceNew(finalSource, "Customer");
        
            std::stringstream toSegStream;
            toSegStream << CtoSegBase << i << j;
            string toSeg = toSegStream.str();
            Ptr<Instance> truckSeg1 = manager->instanceNew(toSeg, "Truck segment");
        
            std::stringstream fromSegStream;
            fromSegStream << CfromSegBase << i << j;
            string fromSeg = fromSegStream.str();
            Ptr<Instance> truckSeg2 = manager->instanceNew(fromSeg, "Truck segment");
        
            truckSeg1->attributeIs("source", finalSource);
            truckSeg2->attributeIs("source", result);
            truckSeg1->attributeIs("return segment", fromSeg);
            
            truckSeg1->attributeIs("length", "10");
	        truckSeg2->attributeIs("length", "10");
	    }
	    
	    std::stringstream toSegStream;
        toSegStream << BtoSegBase << i;
        string toSeg = toSegStream.str();
	    Ptr<Instance> truckSeg1 = manager->instanceNew(toSeg, "Truck segment");
	    
	    std::stringstream fromSegStream;
        fromSegStream << BfromSegBase << i;
        string fromSeg = fromSegStream.str();
	    Ptr<Instance> truckSeg2 = manager->instanceNew(fromSeg, "Truck segment");
	    
	    truckSeg1->attributeIs("source", result);
	    truckSeg2->attributeIs("source", "terminal");
	    truckSeg1->attributeIs("return segment", fromSeg);
	    
	    truckSeg1->attributeIs("length", "10");
	    truckSeg2->attributeIs("length", "10");
	}
	
	for(int i = 0; i < 100; i++) {
        std::stringstream sstm;
        sstm << sourceBase << i;
        string result = sstm.str();
        cout << "Setting path for " << result << endl;
	    Ptr<Instance> curSource = manager->instance(result);
	    
	    curSource->attributeIs("Shipment Size", "100");
	    curSource->attributeIs("Transfer Rate", "2");
	    curSource->attributeIs("Destination", "destination");
	}
	
	for(int i = 0; i < 10; i++) {
	    std::stringstream sstm;
        sstm << terminalBase << i;
        string result = sstm.str();
	    
	    for(int j = 0; j < 10; j++) {
            std::stringstream sstm;
            sstm << finalSourceBase << i << j;
            string finalSource = sstm.str();
            cout << "Setting path for " << finalSource << endl;
            Ptr<Instance> curSource = manager->instance(finalSource);
            
            curSource->attributeIs("Shipment Size", "100");
            curSource->attributeIs("Transfer Rate", "2");
            curSource->attributeIs("Destination", "destination");
	    }
	}
	    

	
	shippingManager->attributeIs("now", "200");
	
	cout << " ------ RESULTS ------ " << endl;
	cout << " Destination packages received : " << destination->attribute("Num Shipments") << endl;
	cout << " Destination average latency : " << destination->attribute("Average Latency") << endl;
	
	cout << "==Selected Segment Stats==" << endl;
	for(int i = 0; i < 5; i++) {
	    std::stringstream toSegStream;
        toSegStream << toSegBase << i;
        string toSeg = toSegStream.str();
        
        Ptr<Instance> segment = manager->instance(toSeg);
        cout << " Segment " << toSeg << " numShipmentsReceived : " << segment->attribute("Shipments Received") << endl;
	}
	
	cout << " Segment termToDest numShipmentsReceived : " << termToDest->attribute("Shipments Received") << endl;
	
	
// 	for(int i = 0; i < 10; i++) {
// 
// 	    for(int j = 0; j < 10; j++) {
//             std::stringstream toSegStream;
//             toSegStream << CtoSegBase << i << j;
//             string toSeg = toSegStream.str();
//             Ptr<Instance> segment = manager->instance(toSeg);
//             
//             cout << "Segment " << toSeg << " numShipmentsReceived : " << segment->attribute("Shipments Received") << endl;
// 	    }
// 	}
	    

  return 0;
}

